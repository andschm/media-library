/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
import {hasAuthority, handleAuthorities} from '../../../main/frontend/common/utils.js'
import store from '../../../main/frontend/common/store.js'

describe('hasAuthority', () => {
    let authorities = [{authority: 'ROLE_USER'}, {authority: 'ROLE_GUEST'}, {authority: 'ROLE_ADMIN'}]

    it('authority exists', () => {
        let authority = 'ROLE_ADMIN'
        expect(hasAuthority(authorities, authority)).toBeTruthy()
    })

    it("authority doesn't exists", () => {
        let authority = 'N/A'
        expect(hasAuthority(authorities, authority)).toBeFalsy()
    })
})

describe('handleAuthorities', () => {
    it('ROLE_USER authority', () => {
        store.commit('reset')
        let authorities = [{authority: 'ROLE_USER'}]
        handleAuthorities(store, authorities)
        expect(store.state.loggedIn).toBeTruthy()
        expect(store.state.navbar.show).toBeTruthy()
        expect(store.state.navbar.showAdmin).toBeFalsy()
        expect(store.state.authorities).toEqual(authorities)
    })

    it('ROLE_ADMIN authority', () => {
        store.commit('reset')
        let authorities = [{authority: 'ROLE_ADMIN'}]
        handleAuthorities(store, authorities)
        expect(store.state.loggedIn).toBeTruthy()
        expect(store.state.navbar.show).toBeTruthy()
        expect(store.state.navbar.showAdmin).toBeTruthy()
        expect(store.state.authorities).toEqual(authorities)
    })
})
