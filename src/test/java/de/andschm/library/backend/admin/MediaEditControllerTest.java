package de.andschm.library.backend.admin;

import de.andschm.library.backend.data.ResultMessage;
import de.andschm.library.backend.service.CategoryService;
import de.andschm.library.backend.service.ImageService;
import de.andschm.library.backend.service.MediaService;
import de.andschm.library.backend.storage.Medium;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mock.web.MockMultipartFile;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MediaEditControllerTest {
	@InjectMocks
	private MediaEditController mediaEditController;

	@Mock
	private MediaService mediaService;

	@Mock
	private CategoryService categoryService;

	@Mock
	private ImageService imageService;

	@Test
	void editMediumNew() {
		final MediumEditData mediumEditData = mediaEditController.editMedium(null);

		assertNotNull(mediumEditData.getMedium());
		assertNotNull(mediumEditData.getCategories());
		assertNull(mediumEditData.getMedium().getId());
	}

	@Test
	void editMedium() {
		final int mediumId = 42;
		final Medium medium = new Medium();
		medium.setId(mediumId);
		when(mediaService.getById(mediumId)).thenReturn(Optional.of(medium));

		final MediumEditData mediumEditData = mediaEditController.editMedium(mediumId);

		assertNotNull(mediumEditData.getMedium());
		assertNotNull(mediumEditData.getCategories());
		assertNotNull(mediumEditData.getMedium().getId());
	}

	@Test
	void saveMedium() {
		final Medium medium = new Medium();
		final MockMultipartFile multipartFile = new MockMultipartFile("data-file", "filename.csv", "text/plain", "some csv".getBytes());
		when(mediaService.saveMedium(any())).then(invocation -> invocation.getArgument(0));

		final ResultMessage resultMessage = mediaEditController.saveMedium(multipartFile, medium);

		assertTrue(resultMessage.isSuccess());
		verify(mediaService, times(2)).saveMedium(any());
	}

	@Test
	void saveMediumWithoutFile() {
		final Medium medium = new Medium();
		final MockMultipartFile multipartFile = new MockMultipartFile("data-file", "", "text/plain", "".getBytes());
		when(mediaService.saveMedium(any())).then(invocation -> invocation.getArgument(0));

		final ResultMessage resultMessage = mediaEditController.saveMedium(multipartFile, medium);

		assertTrue(resultMessage.isSuccess());
		verify(mediaService, times(1)).saveMedium(any());
	}

	@Test
	void saveMediumWithEmptyFile() {
		final Medium medium = new Medium();
		final MockMultipartFile multipartFile = new MockMultipartFile("data-file", "filename.csv", "text/plain", "".getBytes());
		when(mediaService.saveMedium(any())).then(invocation -> invocation.getArgument(0));

		final ResultMessage resultMessage = mediaEditController.saveMedium(multipartFile, medium);

		assertFalse(resultMessage.isSuccess());
		verify(mediaService, times(1)).saveMedium(any());
	}
}
