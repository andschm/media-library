package de.andschm.library.backend.admin;

import de.andschm.library.backend.data.ResultMessage;
import de.andschm.library.backend.service.MediaService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mock.web.MockMultipartFile;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ImportControllerTest {
	@InjectMocks
	private ImportController importController;

	@Mock
	private MediaService mediaService;

	@Test
	void upload() {
		final ClassPathResource importData = new ClassPathResource("library-data.csv");
		final MockMultipartFile multipartFile = new MockMultipartFile("data-file", "filename.csv", "text/plain", "some xml".getBytes());
		when(mediaService.importMediaFromCSV(any())).thenReturn(MediaService.RESULT_SUCCESS);

		final ResultMessage resultMessage = importController.upload(multipartFile);

		assertTrue(resultMessage.isSuccess());
		assertEquals(MediaService.RESULT_SUCCESS, resultMessage.getMessage());
	}

	@Test
	void uploadEmptyFile() {
		final ClassPathResource importData = new ClassPathResource("library-data.csv");
		final MockMultipartFile multipartFile = new MockMultipartFile("data-file", "filename.csv", "text/plain", "".getBytes());

		final ResultMessage resultMessage = importController.upload(multipartFile);

		assertFalse(resultMessage.isSuccess());
		assertEquals("empty", resultMessage.getMessage());
	}

	@Test
	void uploadFailed() {
		final String errorMessage = "error";
		final ClassPathResource importData = new ClassPathResource("library-data.csv");
		final MockMultipartFile multipartFile = new MockMultipartFile("data-file", "filename.csv", "text/plain", "some csv".getBytes());
		when(mediaService.importMediaFromCSV(any())).thenReturn(errorMessage);

		final ResultMessage resultMessage = importController.upload(multipartFile);

		assertFalse(resultMessage.isSuccess());
		assertEquals(errorMessage, resultMessage.getMessage());
	}

}
