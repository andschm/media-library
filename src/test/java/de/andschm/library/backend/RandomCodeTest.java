/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend;

import de.andschm.library.backend.storage.InvitationKey;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class RandomCodeTest {
	@Test
	void createSingleTest() {
		final String code = RandomCode.createSingle(InvitationKey.KEY_SIZE);
		assertEquals(InvitationKey.KEY_SIZE, code.length());
		assertTrue(code.matches("[A-Za-z0-9]*"));
	}

	@Test
	void createMultipleTest() {
		final List<String> codes = RandomCode.createMultiple(InvitationKey.KEY_SIZE, 3);
		assertEquals(3, codes.size());
		for (int i= 0; i < codes.size(); i++) {
			if (i != 0) {
				assertNotEquals(codes.get(0), codes.get(i));
			}
			assertEquals(InvitationKey.KEY_SIZE, codes.get(i).length());
			assertTrue(codes.get(i).matches("[A-Za-z0-9]*"));
		}
	}
}
