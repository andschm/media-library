package de.andschm.library.backend.service;

import de.andschm.library.backend.MediaType;
import de.andschm.library.backend.data.MediumInfo;
import de.andschm.library.backend.storage.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ClassPathResource;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.*;

import static de.andschm.library.backend.service.MediaService.LOCAL_IMAGE_KEY;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class MediaServiceTest {
	@InjectMocks
	private MediaService mediaService;

	@Mock
	private MediaRepository mediaRepository;

	@Mock
	private CategoryRepository categoryRepository;

	@Mock
	private WebPushService pushService;

	@Mock
	private MailService mailService;

	@Mock
	private ApplicationContext applicationContext;

	@Mock
	private PatronRepository patronRepository;

	@BeforeEach
	void before() {
		mediaService.setApplicationContext(applicationContext);
	}

	@Test
	void borrowList() {
		final int mediumId = 1;
		final String author = "Jon";
		final String title = "Title";
		final Patron patron = new Patron();
		final Date releaseDate = new Date();
		final Medium medium = new Medium();
		medium.setId(mediumId);
		medium.setType(MediaType.BOOK);
		medium.setImageUrl(LOCAL_IMAGE_KEY);
		medium.setAuthor(author);
		medium.setTitle(title);
		medium.setLoanedBy(patron);
		medium.setReleaseDate(releaseDate);
		final Date creratedDate = new Date();
		medium.setCreatedDate(creratedDate);

		when(mediaRepository.getByLoanedByIsNotNullOrderByLoanDateDesc()).thenReturn(Collections.singletonList(medium));

		final List<MediumInfo> borrowList = mediaService.borrowList();

		assertEquals(1, borrowList.size());
		final MediumInfo mediumInfo = borrowList.get(0);
		assertEquals(mediumId, mediumInfo.getId());
		assertEquals(MediaType.BOOK, mediumInfo.getType());
		assertEquals("/image/" + mediumId, mediumInfo.getImagePath());
		assertEquals(author, mediumInfo.getAuthor());
		assertEquals(title, mediumInfo.getTitle());
		assertTrue(mediumInfo.isLoanedOut());
		assertEquals(releaseDate, mediumInfo.getReleaseDate());
		assertEquals(creratedDate, mediumInfo.getCreatedDate());
	}

	@Test
	void returnMedium() {
		final Patron patron = new Patron();
		final int mediumId = 1;
		final Medium medium = new Medium();
		medium.setId(mediumId);
		medium.setLoanedBy(patron);
		medium.setLoanDate(new Date());

		when(mediaRepository.findById(mediumId)).thenReturn(Optional.of(medium));

		mediaService.returnMedium(mediumId);

		assertNull(medium.getLoanedBy());
		assertNull(medium.getLoanDate());
	}

	@Test
	void importMediaFromCSV() throws IOException, MessagingException {
		final List<Medium> mediaTable = new LinkedList<>();
		when(mediaRepository.save(any())).then(invocation -> {
			final Medium medium = invocation.getArgument(0);
			mediaTable.add(medium);
			return medium;
		});
		when(categoryRepository.save(any())).then(invocation -> invocation.getArgument(0));

		final ClassPathResource importData = new ClassPathResource("library-data.csv");
		mediaService.importMediaFromCSV(importData.getInputStream());

		assertEquals(2, mediaTable.size());

		final Medium medium0 = mediaTable.get(0);
		assertEquals("Sarah Fielke, Kathy Doughty", medium0.getAuthor());
		assertEquals("us", medium0.getCountryCode());
		assertEquals(0, medium0.getDuration().intValue());
		assertEquals("9781584797524", medium0.getEan());
		assertEquals("Crafts, Hobbies & Home", medium0.getGenre());
		assertEquals("https://images-na.ssl-images-amazon.com/images/I/61klskTIkWL.jpg", medium0.getImageUrl());
		assertEquals("1584797525", medium0.getIsbn());
		assertEquals("English (Published), English (Original Language), English (Unknown)", medium0.getLanguage());
		assertNull(medium0.getLoanDate());
		assertEquals(208, medium0.getNumPages().intValue());
		assertEquals("Kathy Doughty and Sarah Fielke, owners of the popular quilting shop ...", medium0.getOverview());
		assertEquals("", medium0.getOwner());
		assertEquals("Stewart, Tabori and Chang", medium0.getPublisher());
		assertEquals("", medium0.getRevision());
		assertNull(medium0.getStorageLocation());
		assertEquals("Material Obsession: Modern Quilts with Traditional Roots", medium0.getTitle());
		assertEquals(MediaType.BOOK, medium0.getType());
		assertNull(medium0.getLoanedBy());
		assertEquals(2, medium0.getCategories().size());
		assertEquals("neu_20180704", medium0.getCategories().get(0).getName());
		assertEquals("Sachbuch", medium0.getCategories().get(1).getName());

		verify(mediaRepository, times(2)).save(any());
		verify(categoryRepository, times(4)).save(any());
		verify(pushService).sendToAll(any());
		verify(mailService).sendNewMediaMail(any(), any(), eq(mediaTable));
	}
}
