package de.andschm.library.backend.service;

import de.andschm.library.backend.storage.Configuration;
import de.andschm.library.backend.storage.ConfigurationRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ConfigurationServiceTest {
	private final String textKey = "test";
	private final String textValue = "test textValue";
	private final String textDefaultValue = "default";
	private final String numberKey = "nTest";
	private final Long numberValue = 42L;
	private final Long numberDefaultValue = 0L;
	private final String badKey = "nadKey";

	@InjectMocks
	private ConfigurationService configurationService;

	@Mock
	private ConfigurationRepository configurationRepository;

	@Test
	void getTextConfiguration() {
		final Configuration textConfiguration = new Configuration();
		textConfiguration.setName(textKey);
		textConfiguration.setText(textValue);
		when(configurationRepository.findByName(textKey)).thenReturn(Optional.of(textConfiguration));

		final String result = configurationService.getTextConfiguration(textKey, textDefaultValue);

		assertThat("result", result, is(sameInstance(textValue)));
		verify(configurationRepository).findByName(textKey);
	}

	@Test
	void getTextConfigurationDefault() {
		when(configurationRepository.findByName(badKey)).thenReturn(Optional.empty());

		final String result = configurationService.getTextConfiguration(badKey, textDefaultValue);

		assertThat("result", result, is(sameInstance(textDefaultValue)));
		verify(configurationRepository).findByName(badKey);
	}

	@Test
	void getNumberConfiguration() {
		final Configuration numberConfiguration = new Configuration();
		numberConfiguration.setName(numberKey);
		numberConfiguration.setNumber(numberValue);
		when(configurationRepository.findByName(numberKey)).thenReturn(Optional.of(numberConfiguration));

		final Long result = configurationService.getNumberConfiguration(numberKey, numberDefaultValue);

		assertThat("result", result, is(sameInstance(numberValue)));
		verify(configurationRepository).findByName(numberKey);
	}

	@Test
	void getNumberConfigurationDefault() {
		when(configurationRepository.findByName(badKey)).thenReturn(Optional.empty());

		final Long result = configurationService.getNumberConfiguration(badKey, numberDefaultValue);

		assertThat("result", result, is(sameInstance(numberDefaultValue)));
		verify(configurationRepository).findByName(badKey);
	}
}
