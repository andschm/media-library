package de.andschm.library.backend.service;

import de.andschm.library.backend.data.ResultMessage;
import de.andschm.library.backend.storage.Patron;
import de.andschm.library.backend.storage.PatronRepository;
import de.andschm.library.backend.storage.PushSubscription;
import nl.martijndwars.webpush.Notification;
import nl.martijndwars.webpush.PushService;
import org.apache.http.HttpStatus;
import org.apache.http.ProtocolVersion;
import org.apache.http.message.BasicHttpResponse;
import org.jose4j.lang.JoseException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class WebPushServiceTest {
	private WebPushService webPushService;

	@Mock
	private PatronRepository patronRepository;

	@Mock
	private PushService pushService;

	@BeforeEach
	void before() throws GeneralSecurityException {
		final String publicKey = "BPgt_vl6AbTmHRyNdmfXH_O9q36zn_DPwLZTAP_tMAWo-_HcBW-OWjqd-iQnDACz2XEaY_XCR5YpAWKoCB27kM4";
		final String privateKey = "cE-sOHGLO0OytYUPz39jLsXkv1jHNLqFWkchXWPLYWA";
		webPushService = new WebPushService(publicKey, privateKey, patronRepository);
		ReflectionTestUtils.setField(webPushService, "pushService", pushService);
	}

	@Test
	void sendToAll() throws InterruptedException, GeneralSecurityException, JoseException, ExecutionException, IOException {
		final String authOk = "OK";
		final List<Patron> patrons = new LinkedList<>();
		final Patron patron = new Patron();
		final List<PushSubscription> pushSubscriptions1 = new LinkedList<>();
		final PushSubscription pushSubscription1 = new PushSubscription();
		pushSubscription1.setAuth(authOk);
		pushSubscription1.setEndpoint("something");
		pushSubscription1.setP256dh("BNN332xFqA_d5LujqQQnFIGxHlFW662s7uP8pnAxGbMLNv_xtRn4yab4auj6164Au8xF35mGJZH1ZgvbkJDw5vA");
		pushSubscriptions1.add(pushSubscription1);
		final PushSubscription pushSubscription2 = new PushSubscription();
		pushSubscription2.setAuth("");
		pushSubscription2.setEndpoint("something");
		pushSubscription2.setP256dh("BNN332xFqA_d5LujqQQnFIGxHlFW662s7uP8pnAxGbMLNv_xtRn4yab4auj6164Au8xF35mGJZH1ZgvbkJDw5vA");
		pushSubscriptions1.add(pushSubscription2);
		patron.setPushSubscriptions(pushSubscriptions1);
		patrons.add(patron);

		when(pushService.send(any())).then(invocation -> {
			Notification notification = invocation.getArgument(0);
			if (notification.getUserAuth().length > 0) {
				return new BasicHttpResponse(new ProtocolVersion("http", 1, 1), HttpStatus.SC_OK, "OK");
			} else {
				return new BasicHttpResponse(new ProtocolVersion("http", 1, 1), HttpStatus.SC_GONE, "GONE");
			}
		});
		when(patronRepository.findDistinctByPushSubscriptionsNotNull()).thenReturn(patrons);

		final ResultMessage resultMessage = webPushService.sendToAll("message");

		assertTrue(resultMessage.isSuccess());
		assertEquals(1, patron.getPushSubscriptions().size());
	}

}
