package de.andschm.library.backend.service;

import de.andschm.library.backend.data.AccountPageData;
import de.andschm.library.backend.data.NewAccountData;
import de.andschm.library.backend.service.PatronService.ResponseStatus;
import de.andschm.library.backend.storage.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationContext;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.util.ReflectionTestUtils;

import javax.mail.MessagingException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PatronServiceTest {
	@InjectMocks
	private PatronService patronService;

	@Mock
	private PatronRepository patronRepository;

	@Mock
	private MediaRepository mediaRepository;

	@Mock
	private InvitationKeyRepository invitationKeyRepository;

	@Mock
	private ConfigurationService configurationService;

	@Mock
	private MailService mailService;

	@Mock
	private BCryptPasswordEncoder passwordEncoder;

	@Mock
	private DoiService doiService;

	@Mock
	private WebPushService pushService;

	@Mock
	private ApplicationContext applicationContext;

	@BeforeEach
	public void before() {
		patronService.setApplicationContext(applicationContext);
	}

	@Test
	void borrow() throws MessagingException {
		final Date beforeDate = new Date();
		final int mediumId = 1;
		final Medium medium = new Medium();
		medium.setId(mediumId);

		when(mediaRepository.findById(mediumId)).thenReturn(Optional.of(medium));
		final Patron patron = new Patron();
		when(patronRepository.findByEmail(any())).thenReturn(Optional.of(patron));

		final boolean borrowed = patronService.borrow("e-mail-address", mediumId);

		assertTrue(borrowed);
		assertEquals(patron, medium.getLoanedBy());
		assertFalse(beforeDate.after(medium.getLoanDate()));
		verify(mediaRepository).save(medium);
		verify(mailService).sendBorrowMail(eq(patron), any(), eq(medium));
	}

	@Test
	void createPatron() {
		final NewAccountData newAccountData = new NewAccountData();
		newAccountData.setAcceptTerms(true);
		final String email = "someone@acme.com";
		newAccountData.setEmail(email);
		final String invitationKeyValue = "wlkjfhbwiliwföngnwelg";
		final InvitationKey invitationKey = new InvitationKey();
		invitationKey.setKeyValue(invitationKeyValue);
		newAccountData.setInvitationKey(invitationKeyValue);
		final String password = "secret";
		newAccountData.setPassword(password);
		newAccountData.setPasswordRepeat(password);

		when(patronRepository.save(any())).then(invocation -> invocation.getArgument(0));
		when(invitationKeyRepository.findByKeyValue(invitationKeyValue)).thenReturn(Optional.of(invitationKey));

		final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(10);
		ReflectionTestUtils.setField(patronService, "passwordEncoder", passwordEncoder);

		final Patron patron = patronService.createPatron(newAccountData);

		assertEquals(email, patron.getEmail());
		assertTrue(BCrypt.checkpw(password, patron.getPasswordHash()));
		assertEquals(invitationKey.getUsedBy(), patron);
		verify(doiService).sendDoiRequestMail(patron);
	}

	@Test
	void enableUser() {
		final List<InvitationKey> invitationKeys = new LinkedList<>();
		final String doiCode = "1322942056926x1";
		final Patron patron = new Patron();
		patron.setDoiCode(doiCode);

		when(patronRepository.findById(any())).thenReturn(Optional.of(patron));
		when(patronRepository.save(any())).then(invocation -> invocation.getArgument(0));
		final long numKLeys = 3L;
		when(configurationService.getNumberConfiguration(Configuration.NEW_USER_INVITATION_KEYS, 0L)).thenReturn(numKLeys);
		when(invitationKeyRepository.saveAll(any())).then(invocation -> {
			invitationKeys.addAll(invocation.getArgument(0));
			return invocation.getArgument(0);
		});

		ResponseStatus status = patronService.enableUser(doiCode);

		assertEquals(ResponseStatus.OK, status);
		assertTrue(patron.isEnabled());
		assertNull(patron.getDoiCode());
		assertEquals(numKLeys, invitationKeys.size());
		for (InvitationKey key : invitationKeys) {
			assertEquals(patron, key.getOwner());
			assertNotNull(key.getKeyValue());
			assertNull(key.getUsedBy());
		}
	}

	@Test
	void getAccountData() {
		final Patron patron = new Patron();
		final String email = "someone@acme.com";
		final boolean emailNotification = true;
		final List<InvitationKey> invitationKeys = new LinkedList<>();
		final InvitationKey key = new InvitationKey();
		invitationKeys.add(key);
		patron.setEmail(email);
		patron.setEmailNotification(emailNotification);

		when(patronRepository.findByEmail(email)).thenReturn(Optional.of(patron));
		when(invitationKeyRepository.findAllByOwnerAndUsedByIsNull(patron)).thenReturn(invitationKeys);
		when(pushService.getPushPublicKey()).thenReturn("wqöofh3iuohgioevbncwoiuf");

		AccountPageData accountPageData = patronService.getAccountData(email);

		assertEquals(email, accountPageData.getEmail());
		assertEquals(emailNotification, accountPageData.isEmailNotification());
		assertEquals(invitationKeys.size(), accountPageData.getInvitationKeys().size());
		assertTrue(accountPageData.getInvitationKeys()
				.containsAll(invitationKeys.stream().map(InvitationKey::getKeyValue).collect(Collectors.toList())));
		assertEquals(pushService.getPushPublicKey(), accountPageData.getPushPublicKey());
	}
}
