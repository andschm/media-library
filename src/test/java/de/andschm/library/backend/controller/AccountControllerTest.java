package de.andschm.library.backend.controller;

import de.andschm.library.backend.data.EditAccountData;
import de.andschm.library.backend.data.NewAccountData;
import de.andschm.library.backend.data.ResultMessage;
import de.andschm.library.backend.service.PatronService;
import de.andschm.library.backend.service.PatronService.ResponseStatus;
import de.andschm.library.backend.storage.Patron;
import de.andschm.library.backend.storage.PushSubscription;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.Errors;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.LinkedList;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AccountControllerTest {
	private final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

	@InjectMocks
	private AccountController accountController;

	@Mock
	private PatronService patronService;

	@Mock
	private ApplicationContext applicationContext;

	@Mock
	private SecurityContext securityContext;

	@Mock
	private Authentication authentication;

	@BeforeEach
	public void before() {
		ReflectionTestUtils.setField(accountController, "passwordEncoder", passwordEncoder);
		accountController.setApplicationContext(applicationContext);
	}

	@Test
	void newAccount() {
		final NewAccountData newAccountData = new NewAccountData();

		final Errors errors = new BindException(newAccountData, "newAccountData");
		final ResultMessage resultMessage = accountController.newAccount(newAccountData, errors);

		assertTrue(resultMessage.isSuccess());
		assertEquals("OK", resultMessage.getMessage());
		verify(patronService).createPatron(any());
	}

	@Test
	void newAccountFails() {
		final NewAccountData newAccountData = new NewAccountData();
		final Errors errors = new BindException(newAccountData, "newAccountData");
		errors.rejectValue("password", "errorCode");

		final ResultMessage resultMessage = accountController.newAccount(newAccountData, errors);

		assertFalse(resultMessage.isSuccess());
		assertNotEquals("OK", resultMessage.getMessage());
		verify(patronService, times(0)).createPatron(any());
	}

	@Test
	void doiConfirmation() {
		when(patronService.enableUser(any())).thenReturn(ResponseStatus.OK);

		ResultMessage resultMessage = accountController.doiConfirmation("4235664646x1");

		assertTrue(resultMessage.isSuccess());
		verify(patronService).enableUser(any());
	}

	@Test
	void doiConfirmationFailed() {
		when(patronService.enableUser(any())).thenReturn(ResponseStatus.INVALID_DOI_CODE);

		ResultMessage resultMessage = accountController.doiConfirmation("4235664646x1");

		assertFalse(resultMessage.isSuccess());
		verify(patronService).enableUser(any());
	}

	@Test
	void editAccountOK() {
		when(securityContext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securityContext);
		when(patronService.getByEMail(any())).thenReturn(Optional.of(new Patron()));
		final EditAccountData editAccountData = new EditAccountData();
		editAccountData.setEmailNotification(true);
		final Errors errors = new BindException(editAccountData, "editAccountData");

		ResultMessage resultMessage = accountController.editAccount(editAccountData, errors);

		assertTrue(resultMessage.isSuccess());
		assertEquals("OK", resultMessage.getMessage());
		verify(patronService, times(1)).savePatron(any());
	}

	@Test
	void editAccountOkWithPassword() {
		final String email = "someone@acme.com";
		final String password = "secret";
		when(securityContext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securityContext);
		final Patron patron = new Patron();
		patron.setEmail(email);
		patron.setPasswordHash(BCrypt.hashpw(password, BCrypt.gensalt()));
		when(patronService.getByEMail(any())).thenReturn(Optional.of(patron));
		final EditAccountData editAccountData = new EditAccountData();
		editAccountData.setNewPassword("new password");
		editAccountData.setPassword(password);
		final Errors errors = new BindException(editAccountData, "editAccountData");

		ResultMessage resultMessage = accountController.editAccount(editAccountData, errors);

		assertTrue(resultMessage.isSuccess());
		assertEquals("OK", resultMessage.getMessage());
		verify(patronService, times(1)).savePatron(any());
	}

	@Test
	void editAccountNOP() {
		when(securityContext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securityContext);
		when(patronService.getByEMail(any())).thenReturn(Optional.of(new Patron()));
		final EditAccountData editAccountData = new EditAccountData();
		final Errors errors = new BindException(editAccountData, "editAccountData");

		ResultMessage resultMessage = accountController.editAccount(editAccountData, errors);

		assertTrue(resultMessage.isSuccess());
		assertEquals("NOP", resultMessage.getMessage());
		verify(patronService, times(0)).savePatron(any());
	}

	@Test
	void editAccountFailWrongPassword() {
		final String email = "someone@acme.com";
		final String password = "secret";
		when(securityContext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securityContext);
		final Patron patron = new Patron();
		patron.setEmail(email);
		patron.setPasswordHash(BCrypt.hashpw(password, BCrypt.gensalt()));
		when(patronService.getByEMail(any())).thenReturn(Optional.of(patron));
		final EditAccountData editAccountData = new EditAccountData();
		editAccountData.setNewPassword("new password");
		editAccountData.setPassword(password + "qay");
		final Errors errors = new BindException(editAccountData, "editAccountData");

		ResultMessage resultMessage = accountController.editAccount(editAccountData, errors);

		assertFalse(resultMessage.isSuccess());
		assertNotEquals("OK", resultMessage.getMessage());
		verify(patronService, times(0)).savePatron(any());
	}

	@Test
	void subscribePush() {
		when(securityContext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securityContext);
		final Patron patron = new Patron();
		patron.setPushSubscriptions(new LinkedList<>());
		final String email = "someone@acme.com";
		patron.setEmail(email);
		when(patronService.getByEMail(any())).thenReturn(Optional.of(patron));
		final String endpoint = "https://updates.push.services.mozilla.com/wpush/v2/whfiowhfioewfbg";
		final String authKey = "asfewhgtjwgege";
		final String p256dhKey = "wghrjuzkzjdrh5r6uj67kjz";
		final String subscriptionJson =
				"{\"endpoint\":\"" + endpoint + "\",\"keys\":{\"auth\":\"" + authKey + "\",\"p256dh\":\"" + p256dhKey + "\"}}";
		final String encodedSubscriptionJSON = URLEncoder.encode(subscriptionJson, StandardCharsets.UTF_8);

		final ResultMessage resultMessage = accountController.subscribePush(encodedSubscriptionJSON);

		assertTrue(resultMessage.isSuccess());
		assertEquals(1, patron.getPushSubscriptions().size());
		final PushSubscription pushSubscription = patron.getPushSubscriptions().get(0);
		assertEquals(endpoint, pushSubscription.getEndpoint());
		assertEquals(authKey, pushSubscription.getAuth());
		assertEquals(p256dhKey, pushSubscription.getP256dh());
		verify(patronService).savePatron(patron);
	}

}