package de.andschm.library.backend.controller;

import de.andschm.library.backend.data.BorrowData;
import de.andschm.library.backend.data.ResultMessage;
import de.andschm.library.backend.service.PatronService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindException;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MediaControllerTest {
	@InjectMocks
	private MediaController mediaController;

	@Mock
	private PatronService patronService;

	@Mock
	private ApplicationContext applicationContext;

	@Mock
	private SecurityContext securityContext;

	@Mock
	private Authentication authentication;

	@BeforeEach
	public void before() {
		mediaController.setApplicationContext(applicationContext);
	}

	@Test
	void borrow() {
		final String email = "someone@acme.com";
		final BorrowData borrowData = new BorrowData();
		borrowData.setItemId(1);
		when(authentication.getName()).thenReturn(email);
		when(securityContext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securityContext);
		when(patronService.borrow(any(), any())).thenReturn(true);

		final ResultMessage resultMessage = mediaController.borrow(borrowData, new BindException(borrowData, "borrowData"));

		assertTrue(resultMessage.isSuccess());
	}

	@Test
	void borrowFail() {
		final String email = "someone@acme.com";
		final BorrowData borrowData = new BorrowData();
		borrowData.setItemId(1);
		when(authentication.getName()).thenReturn(email);
		when(securityContext.getAuthentication()).thenReturn(authentication);
		SecurityContextHolder.setContext(securityContext);
		when(patronService.borrow(any(), any())).thenReturn(false);

		final ResultMessage resultMessage = mediaController.borrow(borrowData, new BindException(borrowData, "borrowData"));

		assertFalse(resultMessage.isSuccess());
	}

}
