package de.andschm.library.backend;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CodingTest {

	@Test
	void bytesToHex() {
		final byte[] testData = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 42, 77};
		final String hexResult = Coding.bytesToHex(testData);
		assertEquals("0102030405060708090A0B0C0D0E0F102A4D", hexResult);
	}
}
