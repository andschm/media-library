/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
import Vue from 'vue'
import App from './App.vue'
import i18next from 'i18next'
import VueI18Next from '@panter/vue-i18next'
import XHR from 'i18next-xhr-backend'
import VueRouter from 'vue-router'
import store from '../common/store.js'
import MediaList from './components/MediaList.vue'
import Account from './components/Account.vue'
import Login from './components/Login.vue'
import Register from './components/Register.vue'
import Registered from './components/Registered.vue'
import Help from './components/Help.vue'
import RegisterConfirmation from './components/RegisterConfirmation.vue'
import DataPrivacy from './components/DataPrivacy.vue'
import NewArrivals from './components/NewArrivals.vue'

let userLang = navigator.language || navigator.userLanguage

// only use Primary Language Subtag
userLang = userLang.substring(0, userLang.indexOf('-'))

Vue.use(VueRouter)
Vue.use(VueI18Next)
let vueEventBus = new Vue()

i18next
    .use(XHR)
    .init({
        // 'debug': true,
        ns: ['common', 'main'],
        defaultNS: 'common',
        lng: userLang,
        fallbackLng: 'de',
        backend: {
            loadPath: '/static/locales/{{lng}}/{{ns}}.json'
        }
    }, (err, t) => {
        if (err) {
            console.log('something went wrong loading', err)
        } else {
            vueEventBus.$emit('i18n-loaded')
        }
        document.title = i18n.t('common:application.title')
    })

const i18n = new VueI18Next(i18next)

Vue.filter('formatDate', function (value) {
    if (value) {
        var date = new Date(value)
        return date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear()
    }
})

let router = new VueRouter({
    mode: 'history',
    routes: [
        {
            name: 'mediaList',
            component: MediaList,
            path: '/'
        },
        {
            name: 'Login',
            component: Login,
            path: '/login'
        },
        {
            name: 'Account',
            component: Account,
            path: '/account'
        },
        {
            name: 'Register',
            component: Register,
            path: '/register'
        },
        {
            name: 'Registered',
            component: Registered,
            path: '/registered'
        },
        {
            name: 'Help',
            component: Help,
            path: '/help'
        },
        {
            name: "RegisterConfirmation",
            component: RegisterConfirmation,
            path:"/doiConfirmation"
        },
        {
            name: "DataPrivacy",
            component: DataPrivacy,
            path:"/datenschutz"
        },
        {
            name: "NewArrivals",
            component: NewArrivals,
            path:"/newArrivals"
        }
    ]
})

Vue.prototype.$vueEventBus =vueEventBus

new Vue({
    i18n: i18n,
    el: '#app',
    store,
    router,
    render: h => h(App)
})

export default i18n
