/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
const checkRights = function (that) {
    that.ajaxActive = true
    $.ajax({
        url: '/api/v1/security/default-target',
        method: 'GET',
        xhrFields: {
            withCredentials: true
        },
        success: function (result) {
            handleAuthorities(that.$store, result.authorities)
            that.ajaxActive = false
            if (!that.$store.state.loggedIn) {
                location.href = '/login'
            }
        },
        error: function (jqXHR) {
            that.ajaxActive = false
            console.log(jqXHR.status)
        }
    })
}

const handleAuthorities = function ($store, authorities) {
    $store.commit('setAuthorities', authorities)
    if (authorities.length > 0) {
        if (hasAuthority(authorities, 'ROLE_USER')) {
            $store.commit('showNavbar', true)
            $store.commit('setLoggedIn', true)
        }
        if (hasAuthority(authorities, 'ROLE_ADMIN')) {
            $store.commit('showNavbar', true)
            $store.commit('showAdminNavbar', true)
            $store.commit('setLoggedIn', true)
        }
    }
}

const hasAuthority = function(authorities, authority) {
    for (var i = 0; i < authorities.length; i++) {
        if (authorities[i].authority === authority) {
            return true
        }
    }
    return false
}

export { checkRights, handleAuthorities, hasAuthority }
