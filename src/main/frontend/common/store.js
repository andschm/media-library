/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        uninitialized: true,
        loggedIn: false,
        navbar: {
            show: false,
            showAdmin: false
        },
        authorities: []
    },
    mutations: {
        setLoggedIn (state, value) {
            state.loggedIn = value
            state.uninitialized = false
        },
        showNavbar (state, value) {
            state.navbar.show = value
            state.uninitialized = false
        },
        showAdminNavbar (state, value) {
            state.navbar.showAdmin = value
            state.uninitialized = false
        },
        setAuthorities (state, value) {
            state.authorities = value
            state.uninitialized = false
        },
        reset (state) {
            state.loggedIn = false
            state.navbar.show = false
            state.navbar.showAdmin = false
            state.authorities = []
            state.uninitialized = true
        }
    }
})

export default store
