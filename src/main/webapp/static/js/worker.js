/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
'use strict';

self.addEventListener('push', ev => {
    const title = 'Neuigkeitem aus der Bibliothek';
    const options = {
        body: ev.data.text(),
        icon: '../images/library-10.png'
    };
    ev.waitUntil(self.registration.showNotification(title, options));
});

self.addEventListener('notificationclick', ev => {
    ev.notification.close();
    ev.waitUntil(clients.openWindow('https://bibliothek.cargodeck.de/'));
});
