/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library;

import de.andschm.library.backend.security.AppUserDetailsService;
import de.andschm.library.backend.security.Roles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	private final BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				.csrf().disable()
				.authorizeRequests()
				.antMatchers("/api/v1/security/**", "/api/v1/account/new", "/api/v1/account/doiConfirmation/**")
				.permitAll()
				.antMatchers("/api/v1/admin/**", "/admin/**")
				.hasAuthority(Roles.ROLE_ADMIN.name())
				.antMatchers("/api/**")
				.hasAnyAuthority(Roles.ROLE_USER.name(), Roles.ROLE_ADMIN.name())
				.anyRequest()
				.permitAll()
				.and()
				.logout()
				.logoutUrl("/api/v1/security/logout")
				.and()
				.formLogin()
				.loginProcessingUrl("/api/v1/security/login-processing")
				.loginPage("/api/v1/security/login-page")
				.failureUrl("/api/v1/security/authentication-failure")
				.defaultSuccessUrl("/api/v1/security/default-target", false)
				.and()
				.httpBasic()
				.and()
				.cors()
				.and()
				.headers()
				.xssProtection()
		;
	}

	@Override
	public void configure(WebSecurity web) {
		web.ignoring().antMatchers("/static/**", "/status", "/image/**", "/management/health");
	}

	@Autowired
	public void configureGlobal(final AuthenticationManagerBuilder auth, final AppUserDetailsService appUserDetailsService)
			throws Exception {
		auth.userDetailsService(appUserDetailsService).passwordEncoder(passwordEncoder);
	}

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return passwordEncoder;
	}

}