/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library;

import de.andschm.library.backend.FileTypeUtil;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@EnableJpaAuditing
@EnableTransactionManagement
@EnableSpringDataWebSupport
public class MediaLibraryApplication extends SpringBootServletInitializer {
	private static final String DEFAULT_ENCODING = "UTF-8";

//	@Override
//	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
//		return application.sources(MediaLibraryApplication.class);
//	}

	@Bean
	MessageSource messageSource() {
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("classpath:messages");
		messageSource.setDefaultEncoding(DEFAULT_ENCODING);
		return messageSource;
	}

	@Bean
	FileTypeUtil imageFileTypeUtil() {
		final FileTypeUtil fileTypeUtil = new FileTypeUtil(0);
		final Map<String, String> types = new HashMap<>();
		types.put("png",	"$89504E470D0A1A0A");
//		types.put("tif",	"$4949002A");
//		types.put("tiff",	"$4949002A");
		types.put("tif",	"$4D4D002A");
		types.put("tiff",	"$4D4D002A");
		types.put("jpg",	"$FFD8");
		types.put("jpeg",	"$FFD8");
		fileTypeUtil.setTypes(types);
		return fileTypeUtil;
	}

	@Bean
	public ResourceBundleMessageSource emailMessageSource() {
		final ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
		messageSource.setDefaultEncoding(DEFAULT_ENCODING);
		messageSource.setBasename("messages");
		return messageSource;
	}

	@Bean
	public TemplateEngine emailTemplateEngine(final ApplicationContext context) {
		final SpringTemplateEngine templateEngine = new SpringTemplateEngine();
		// Resolver for TEXT emails
		templateEngine.addTemplateResolver(textTemplateResolver(context));
		// Resolver for HTML emails (except the editable one)
		templateEngine.addTemplateResolver(htmlTemplateResolver(context));
		// Message source, internationalization specific to emails
		templateEngine.setTemplateEngineMessageSource(emailMessageSource());
		return templateEngine;
	}

	private ITemplateResolver textTemplateResolver(final ApplicationContext context) {
		final SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();
		templateResolver.setApplicationContext(context);
		templateResolver.setOrder(1);
		templateResolver.setResolvablePatterns(Collections.singleton("text/*"));
		templateResolver.setPrefix("classpath:/templates/mail/");
		templateResolver.setSuffix(".txt");
		templateResolver.setCharacterEncoding(DEFAULT_ENCODING);
		templateResolver.setCacheable(false);
//		templateResolver.afterPropertiesSet();
		return templateResolver;
	}

	private ITemplateResolver htmlTemplateResolver(final ApplicationContext context) {
		final SpringResourceTemplateResolver templateResolver = new SpringResourceTemplateResolver();
		templateResolver.setApplicationContext(context);
		templateResolver.setOrder(2);
		templateResolver.setResolvablePatterns(Collections.singleton("html/*"));
		templateResolver.setPrefix("classpath:/templates/mail/");
		templateResolver.setSuffix(".html");
//		templateResolver.setTemplateMode(StandardTemplateModeHandlers.HTML5.getTemplateModeName());
		templateResolver.setCharacterEncoding(DEFAULT_ENCODING);
		templateResolver.setCacheable(false);
//		templateResolver.afterPropertiesSet();
		return templateResolver;
	}

	public static void main(final String[] args) {
		SpringApplication.run(MediaLibraryApplication.class, args);
	}
}
