/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.admin;

import de.andschm.library.backend.data.ResultMessage;
import de.andschm.library.backend.service.CategoryService;
import de.andschm.library.backend.service.ImageService;
import de.andschm.library.backend.service.MediaService;
import de.andschm.library.backend.storage.Medium;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

import static de.andschm.library.backend.service.MediaService.LOCAL_IMAGE_KEY;

/**
 * Controller for editing, adding media
 * <p>
 * Created by andschm on 06.05.2017.
 */
@RestController
@RequestMapping("/api/v1/admin/editmedium")
public class MediaEditController {
	private final Log log = LogFactory.getLog(this.getClass());
	private final MediaService mediaService;
	private final ImageService imageService;
	private final CategoryService categoryService;

	public MediaEditController(final MediaService mediaService, final ImageService imageService, final CategoryService categoryService) {
		this.mediaService = mediaService;
		this.imageService = imageService;
		this.categoryService = categoryService;
	}

	@GetMapping()
	public MediumEditData editMedium(@RequestParam(required = false) final Integer id) {
		final Medium medium = id != null ? mediaService.getById(id).orElseGet(Medium::new) : new Medium();
		return new MediumEditData(medium, categoryService.getAllCategories());
	}

	@PostMapping()
	public ResultMessage saveMedium(@RequestParam("image") final MultipartFile file, @Valid final Medium medium) {

		String message;
		boolean success = false;

		final Medium savedMedium = mediaService.saveMedium(medium);
		if (!file.isEmpty()) {
			final String uploadError = imageService.handleImageUpload(file, savedMedium);
			if (StringUtils.isEmpty(uploadError)) {
				savedMedium.setImageUrl(LOCAL_IMAGE_KEY);
				mediaService.saveMedium(savedMedium);
				message = "ok";
				success = true;
			} else {
				message = uploadError;
			}
		} else {
			if (StringUtils.isNotBlank(file.getOriginalFilename())) {
				message = "upload.image.empty";
			} else {
				message = "ok";
				success = true;
			}
		}
		return new ResultMessage(success, message);
	}

	@ExceptionHandler(MultipartException.class)
	@ResponseBody
	public String multipartException(MultipartException e) {
		log.warn("SizeLimitExceededException", e);
		return "ERROR";
	}

	@ExceptionHandler(MaxUploadSizeExceededException.class)
	@ResponseBody
	public String maxUploadSizeExceededException(MaxUploadSizeExceededException e) {
		log.warn("SizeLimitExceededException", e);
		return "ERROR";
	}

}
