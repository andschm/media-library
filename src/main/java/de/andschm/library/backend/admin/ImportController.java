/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.admin;

import de.andschm.library.backend.data.ResultMessage;
import de.andschm.library.backend.service.MediaService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedInputStream;
import java.io.IOException;

/**
 * Controller for uploading csv data
 * <p>
 * Created by andschm on 30.04.2017.
 */
@RestController
@RequestMapping("/api/v1/admin/import")
public class ImportController {
	private final Log log = LogFactory.getLog(this.getClass());

	private final MediaService mediaService;

	public ImportController(final MediaService mediaService) {
		this.mediaService = mediaService;
	}

	@PostMapping()
	public ResultMessage upload(@RequestParam("data-file") final MultipartFile file) {
		String message;
		boolean success = false;

		if (!file.isEmpty()) {
			try {
				final BufferedInputStream input = new BufferedInputStream(file.getInputStream());
				message = mediaService.importMediaFromCSV(input);
				if (MediaService.RESULT_SUCCESS.equals(message)) {
					success = true;
					log.info("upload: success");
				}
			} catch (IOException e) {
				message = "storing";
				log.error("upload: storing file failed", e);
			}
		} else {
			message = "empty";
			log.info("upload: uploaded file is empty");
		}

		return new ResultMessage(success, message);
	}

}
