/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.admin;

import de.andschm.library.backend.data.MediumInfo;
import de.andschm.library.backend.service.MediaService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Admin controller for handling borrowed media.
 * <p>
 * Created by andschm on 21.05.2017.
 */
@RestController
@RequestMapping("/api/v1/admin/borrow")
public class BorrowedMediaController {
	private final MediaService mediaService;

	public BorrowedMediaController(MediaService mediaService) {
		this.mediaService = mediaService;
	}

	@GetMapping("list")
	public List<MediumInfo> listBorrowedMedia() {
		return mediaService.borrowList();
	}

	@GetMapping("return/{id}")
	public void returnBorrowedMedium(@PathVariable final Integer id, final HttpServletResponse response) throws IOException {
		mediaService.returnMedium(id);
		response.sendRedirect("../list");
	}

}
