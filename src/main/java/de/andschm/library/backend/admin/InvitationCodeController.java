/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.admin;

import de.andschm.library.backend.service.InvitationCodeService;
import de.andschm.library.backend.storage.InvitationKey;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Controller for managing invitation codes
 * <p>
 * Created by andschm on 07.01.2018.
 */
@RestController
@RequestMapping("/api/v1/admin/invitationcodes")
public class InvitationCodeController {
	private final InvitationCodeService invitationCodeService;

	public InvitationCodeController(final InvitationCodeService invitationCodeService) {
		this.invitationCodeService = invitationCodeService;
	}

	@GetMapping("/list")
	public List<InvitationKey> list() {
		return invitationCodeService.getUnusedPublicCodes();
	}

	@GetMapping("/generate")
	public void generate(final HttpServletResponse response) throws IOException {
		invitationCodeService.generatePublicCode();
		response.sendRedirect("list");
	}
}
