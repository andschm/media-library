package de.andschm.library.backend.admin;

import de.andschm.library.backend.storage.Category;
import de.andschm.library.backend.storage.Medium;

import java.util.List;

public class MediumEditData {
	private Medium medium;
	private List<Category> categories;

	public MediumEditData(final Medium medium, final List<Category> categories) {
		this.medium = medium;
		this.categories = categories;
	}

	public Medium getMedium() {
		return medium;
	}

	public void setMedium(Medium medium) {
		this.medium = medium;
	}

	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}
}
