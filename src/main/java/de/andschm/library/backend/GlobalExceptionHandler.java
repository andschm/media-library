/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend;

import org.apache.tomcat.util.http.fileupload.impl.FileSizeLimitExceededException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
public class GlobalExceptionHandler {

	//StandardServletMultipartResolver
	@ExceptionHandler(MultipartException.class)
	public void handleError1(MultipartException e, RedirectAttributes redirectAttributes, final HttpServletRequest request, final
	HttpServletResponse response)
			throws IOException {
		redirectAttributes.addFlashAttribute("message", e.getCause().getMessage());
		Throwable ex = e;
		while (ex.getCause() != null && ex.getClass() != FileSizeLimitExceededException.class) {
			ex = ex.getCause();
		}
		if (((FileSizeLimitExceededException) ex).getFieldName().equals("image")) {
			response.sendRedirect(request.getContextPath() + request.getServletPath());
		}
		response.sendRedirect(request.getContextPath() + request.getServletPath());
	}

	//CommonsMultipartResolver
	@ExceptionHandler(MaxUploadSizeExceededException.class)
	public String handleError2(MaxUploadSizeExceededException e, RedirectAttributes redirectAttributes) {
		redirectAttributes.addFlashAttribute("message", e.getCause().getMessage());
		return "redirect:/uploadStatus";
	}

}
