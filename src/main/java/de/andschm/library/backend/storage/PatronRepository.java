/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.storage;

import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;
import java.util.Optional;

public interface PatronRepository extends PagingAndSortingRepository<Patron, Integer> {
	Optional<Patron> findByEmail(final String email);

	Optional<Patron> findByIdAndDoiCode(final int userId, final String doiCode);

	Optional<Patron> findByEmailAndAccountLocked(final String email, boolean locked);

	List<Patron> findDistinctByPushSubscriptionsNotNull();

	List<Patron> findByEmailNotificationTrue();
}
