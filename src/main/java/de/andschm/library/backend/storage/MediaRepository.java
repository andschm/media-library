/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.storage;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface MediaRepository extends PagingAndSortingRepository<Medium, Integer> {
    Page<Medium> findAllByTitleContainsOrOverviewContainsOrAuthorContainsOrPublisherContainsOrderByTitle(
            final Pageable pageable,
            final String title,
            final String overview,
            final String author,
            final String publisher);

    List<Medium> getByLoanedByIsNotNullOrderByLoanDateDesc();

    Optional<Medium> findFirstByOrderByCreatedDateDesc();

    List<Medium> getByCreatedDateAfterOrderByTitle(final Date earliestCreatedDate);
}
