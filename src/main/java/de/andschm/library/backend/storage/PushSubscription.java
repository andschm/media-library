package de.andschm.library.backend.storage;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class PushSubscription {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String endpoint;
	private String p256dh;
	private String auth;

	@CreatedDate
	@Column(name = "createdDate", columnDefinition = "DATETIME(3) NOT NULL")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@LastModifiedDate
	@Column(name = "lastModifiedDate", columnDefinition = "DATETIME(3) NOT NULL")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModifiedDate;

	public PushSubscription() {
		// No-args constructor
	}

	public PushSubscription(final String endpoint, final String p256dh, final String auth) {
		this.endpoint = endpoint;
		this.p256dh = p256dh;
		this.auth = auth;
	}

	public Integer getId() {
		return id;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String getP256dh() {
		return p256dh;
	}

	public void setP256dh(String p256dh) {
		this.p256dh = p256dh;
	}

	public String getAuth() {
		return auth;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}
}
