/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.storage;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import java.util.Date;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class InvitationKey {
	public static final int KEY_SIZE = 50;
	@Id
	@Size(max = KEY_SIZE)
	private String keyValue;

	@ManyToOne
	private Patron usedBy;

	@ManyToOne
	private Patron owner;

	@CreatedDate
	@Column(name = "createdDate", columnDefinition = "DATETIME(3) NOT NULL")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@LastModifiedDate
	@Column(name = "lastModifiedDate", columnDefinition = "DATETIME(3) NOT NULL")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModifiedDate;

	public boolean isUnused() {
		return usedBy == null;
	}

	public String getKeyValue() {
		return keyValue;
	}

	public void setKeyValue(String keyValue) {
		this.keyValue = keyValue;
	}

	public Patron getUsedBy() {
		return usedBy;
	}

	public void setUsedBy(Patron usedBy) {
		this.usedBy = usedBy;
	}

	public Patron getOwner() {
		return owner;
	}

	public void setOwner(Patron owner) {
		this.owner = owner;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}
}
