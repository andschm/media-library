/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.storage;

import de.andschm.library.backend.security.Roles;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@EntityListeners(AuditingEntityListener.class)
public class Patron {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String email;
    private String passwordHash;
    private Roles role;
	private boolean enabled;
	private boolean accountExpired;
	private boolean accountLocked;
	private boolean passwordExpired;

    private String doiCode;
    private boolean emailNotification;

	@OneToMany(
			cascade = CascadeType.ALL,
			orphanRemoval = true
	)
	private List<PushSubscription> pushSubscriptions;

	@Temporal(TemporalType.TIMESTAMP)
	private Date lastLogin;

	@CreatedDate
	@Column(name = "createdDate", columnDefinition = "DATETIME(3) NOT NULL")
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@LastModifiedDate
	@Column(name = "lastModifiedDate", columnDefinition = "DATETIME(3) NOT NULL")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastModifiedDate;

    public Patron() {
        // default constructor
    }

    public Patron(final String email, final String passwordHash) {
        this.email = email;
        this.passwordHash = passwordHash;
    }

	public void addPushSubscription(PushSubscription pushSubscription) {
		this.pushSubscriptions.add(pushSubscription);
	}

    public Integer getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

	public String getPasswordHash() {
		return passwordHash;
	}

	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash;
	}

	public Roles getRole() {
		return role;
	}

	public void setRole(Roles role) {
		this.role = role;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public boolean isAccountExpired() {
		return accountExpired;
	}

	public void setAccountExpired(boolean accountExpired) {
		this.accountExpired = accountExpired;
	}

	public boolean isAccountLocked() {
		return accountLocked;
	}

	public void setAccountLocked(boolean accountLocked) {
		this.accountLocked = accountLocked;
	}

	public boolean isPasswordExpired() {
		return passwordExpired;
	}

	public void setPasswordExpired(boolean passwordExpired) {
		this.passwordExpired = passwordExpired;
	}

	public String getDoiCode() {
		return doiCode;
	}

	public void setDoiCode(String doiCode) {
		this.doiCode = doiCode;
	}

	public Date getCreatedDate() {
        return createdDate;
    }

    public Date getLastModifiedDate() {
        return lastModifiedDate;
    }

	public boolean isEmailNotification() {
		return emailNotification;
	}

	public void setEmailNotification(boolean emailNotification) {
		this.emailNotification = emailNotification;
	}

	public List<PushSubscription> getPushSubscriptions() {
		return pushSubscriptions;
	}

	public void setPushSubscriptions(List<PushSubscription> pushSubscriptions) {
		this.pushSubscriptions = pushSubscriptions;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(final Date lastLogin) {
		this.lastLogin = lastLogin;
	}
}
