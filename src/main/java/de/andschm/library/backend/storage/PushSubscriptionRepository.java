package de.andschm.library.backend.storage;

import org.springframework.data.repository.CrudRepository;

public interface PushSubscriptionRepository extends CrudRepository<PushSubscription, Integer> {
}
