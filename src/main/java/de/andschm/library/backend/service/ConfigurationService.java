package de.andschm.library.backend.service;

import de.andschm.library.backend.storage.Configuration;
import de.andschm.library.backend.storage.ConfigurationRepository;
import org.springframework.stereotype.Component;

@Component
public class ConfigurationService {
	private final ConfigurationRepository configurationRepository;

	public ConfigurationService(final ConfigurationRepository configurationRepository) {
		this.configurationRepository = configurationRepository;
	}

	public String getTextConfiguration(final String key, final String defaultValue) {
		return configurationRepository.findByName(key).map(Configuration::getText).orElse(defaultValue);
	}

	public Long getNumberConfiguration(final String key, final Long defaultValue) {
		return configurationRepository.findByName(key).map(Configuration::getNumber).orElse(defaultValue);
	}
}
