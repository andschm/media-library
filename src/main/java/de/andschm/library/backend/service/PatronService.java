/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.service;

import de.andschm.library.backend.RandomCode;
import de.andschm.library.backend.data.AccountPageData;
import de.andschm.library.backend.data.NewAccountData;
import de.andschm.library.backend.security.Roles;
import de.andschm.library.backend.storage.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

@Component
public class PatronService implements ApplicationContextAware {
	public enum ResponseStatus {
		OK, NO_USER, NO_DOI_CODE, INVALID_DOI_CODE, ALREADY_CONFIRMED
	}
	private final Log log = LogFactory.getLog(this.getClass());
	private final PatronRepository patronRepository;
	private final MediaRepository mediaRepository;
	private final InvitationKeyRepository invitationKeyRepository;
	private final ConfigurationService configurationService;
	private final MailService mailService;
	private final PasswordEncoder passwordEncoder;
	private final DoiService doiService;
	private final WebPushService webPushService;
	private ApplicationContext applicationContext;

	public PatronService(
			final PatronRepository patronRepository,
			final MediaRepository mediaRepository,
			final InvitationKeyRepository invitationKeyRepository,
			final ConfigurationService configurationService,
			final MailService mailService,
			final PasswordEncoder passwordEncoder,
			final DoiService doiService,
			final WebPushService webPushService) {

		this.patronRepository = patronRepository;
		this.mediaRepository = mediaRepository;
		this.invitationKeyRepository = invitationKeyRepository;
		this.configurationService = configurationService;
		this.mailService = mailService;
		this.passwordEncoder = passwordEncoder;
		this.doiService = doiService;
		this.webPushService = webPushService;
	}

	public Optional<Patron> getByEMail(final String eMail) {
		return patronRepository.findByEmail(eMail);
	}

	public void savePatron(final Patron patron) {
		patronRepository.save(patron);
	}

	public boolean borrow(final String eMail, final Integer mediumId) {
		final var result = new AtomicBoolean(false);
		final var mediumOptional = mediaRepository.findById(mediumId);
		mediumOptional.ifPresent(medium -> {
			final var patronOptional = getByEMail(eMail);
			patronOptional.ifPresent(patron -> {
				medium.setLoanedBy(patron);
				medium.setLoanDate(new Date());
				mediaRepository.save(medium);
				result.set(true);
				try {
					final var subject = applicationContext.getMessage("email.borrow.subject", null, LocaleContextHolder.getLocale());
					mailService.sendBorrowMail(patron, subject, medium);
				} catch (MessagingException e) {
					log.warn("borrow: error sending mail", e);
				}
			});
		});
		return result.get();
	}

	public Patron createPatron(final NewAccountData newAccountData) {
		final var patron = new Patron(newAccountData.getEmail(), passwordEncoder.encode(newAccountData.getPassword()));
		patron.setRole(Roles.ROLE_USER);
		final var savedPatron = patronRepository.save(patron);

		doiService.sendDoiRequestMail(savedPatron);

		if (StringUtils.isNotEmpty(newAccountData.getInvitationKey())) {
			final var invitationKeyOptional = invitationKeyRepository.findByKeyValue(newAccountData.getInvitationKey());
			invitationKeyOptional.ifPresent(invitationKey -> {
				invitationKey.setUsedBy(savedPatron);
				invitationKeyRepository.save(invitationKey);
			});
		}
		return savedPatron;
	}

	public ResponseStatus enableUser(final String doiCode) {
		final var result = new AtomicReference<>(ResponseStatus.NO_USER);
		final var userId = extractUserIdFromDoiCode(doiCode);
		final var patronOptional = patronRepository.findById(userId);
		patronOptional.ifPresent(patron -> {
			if (patron.isEnabled()) {
				result.set(ResponseStatus.ALREADY_CONFIRMED);
			} else if (StringUtils.isBlank(patron.getDoiCode())) {
				result.set(ResponseStatus.NO_DOI_CODE);
			} else if (!doiCode.equals(patron.getDoiCode())) {
				result.set(ResponseStatus.INVALID_DOI_CODE);
			} else {
				patron.setEnabled(true);
				patron.setDoiCode(null);
				final var savedPatron = patronRepository.save(patron);
				createInvitationKeysForPatron(savedPatron);
				result.set(ResponseStatus.OK);
			}
		});
		return result.get();
	}

	private int extractUserIdFromDoiCode(final String doiCode) {
		final var pos = doiCode.lastIndexOf('x');
		if (pos < 0) {
			return -1;
		}
		final var userId = doiCode.substring(pos + 1);
		return Integer.parseInt(userId);
	}

	private void createInvitationKeysForPatron(final Patron patron) {
		final var numInvitationKeys = configurationService.getNumberConfiguration(Configuration.NEW_USER_INVITATION_KEYS, 0L);
		if (numInvitationKeys > 0L) {
			final var invitationKeys = createInvitationKeysForPatron(patron, numInvitationKeys);
			invitationKeyRepository.saveAll(invitationKeys);
		}
	}

	private List<InvitationKey> createInvitationKeysForPatron(final Patron user, final long count) {
		final var invitationKeys = new LinkedList<InvitationKey>();
		final var codes = RandomCode.createMultiple(InvitationKey.KEY_SIZE, count);
		for (final var code: codes) {
			final var invitationKey = new InvitationKey();
			invitationKey.setKeyValue(code);
			invitationKey.setOwner(user);
			invitationKeys.add(invitationKey);
		}

		return invitationKeys;
	}

	public AccountPageData getAccountData(final String eMail) {
		final var accountPageData = new AccountPageData();
		accountPageData.setEmail(eMail);
		final var patronOptional = getByEMail(eMail);
		patronOptional.ifPresent(patron -> {
			accountPageData.setEmailNotification(patron.isEmailNotification());
			final var invitationKeysAsString = new LinkedList<String>();
			final var invitationKeys = invitationKeyRepository.findAllByOwnerAndUsedByIsNull(patron);
			for (final var key: invitationKeys) {
				invitationKeysAsString.add(key.getKeyValue());
			}
			accountPageData.setInvitationKeys(invitationKeysAsString);
			accountPageData.setPushPublicKey(webPushService.getPushPublicKey());
		});
		return accountPageData;
	}

	@Override
	public void setApplicationContext(final ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
