/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.service;

import de.andschm.library.backend.Coding;
import de.andschm.library.backend.FileTypeUtil;
import de.andschm.library.backend.storage.Image;
import de.andschm.library.backend.storage.ImageRepository;
import de.andschm.library.backend.storage.Medium;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class ImageService {
	private final Log log = LogFactory.getLog(this.getClass());

	private List<String> fileExtensions;
	private FileTypeUtil fileTypeUtil;
	private ImageRepository imageRepository;

	public ImageService(
			final FileTypeUtil fileTypeUtil,
			final ImageRepository imageRepository,
			@Value("${image.fileExtensions}") final String fileExtensionsConfig) {

		this.fileTypeUtil = fileTypeUtil;
		this.imageRepository = imageRepository;

		fileExtensions = new ArrayList<>(Arrays.asList(fileExtensionsConfig.split(",")));
	}

	public String handleImageUpload(final MultipartFile file, final Medium medium) {
		String uploadResponse = "";
		final String originalFilename = file.getOriginalFilename();
		try {
			final BufferedInputStream input = new BufferedInputStream(file.getInputStream());
			if (checkFileFormat(input, originalFilename)) {
				final List<Image> oldImages = imageRepository.findByMedium(medium);
				final Image image = new Image();
				image.setMedium(medium);
				byte[] bytes = IOUtils.toByteArray(input);
				image.setData(bytes);
				image.setOriginalFileName(originalFilename);
				imageRepository.save(image);
				imageRepository.deleteAll(oldImages);
			} else {
				uploadResponse = "upload.image.wrongFormat";
			}
		} catch (IOException e) {
			log.error("handleImageUpload: storing image failed - " + originalFilename, e);
			uploadResponse = "upload.image.failed";
		}
		return uploadResponse;
	}

	private boolean checkFileFormat(final BufferedInputStream input, final String originalFilename) throws IOException {
		final String extension = FilenameUtils.getExtension(originalFilename);
		if (fileExtensions.contains(extension.toLowerCase())) {
			final int bytesToRead = fileTypeUtil.getMaxHeaderLength();
			input.mark(bytesToRead);
			final byte[] topOfstream = new byte[bytesToRead];
			input.read(topOfstream);
			input.reset();
			if (fileTypeUtil.checkType(topOfstream)) {
				return true;
			} else {
				log.info("checkFileFormat: uploaded image '" + originalFilename + "' has wrong format: " + Coding.bytesToHex(topOfstream));
				return false;
			}
		} else {
			log.info("checkFileFormat: uploaded image has wrong extension: " + originalFilename);
			return false;
		}
	}

	public Image getImageForMedium(final Medium medium) {
		final List<Image> images = imageRepository.findByMedium(medium);
		if (images.isEmpty()) {
			return null;
		} else {
			return images.get(0);
		}
	}
}
