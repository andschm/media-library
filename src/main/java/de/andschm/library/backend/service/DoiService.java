/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.service;

import de.andschm.library.backend.RandomCode;
import de.andschm.library.backend.storage.Patron;
import de.andschm.library.backend.storage.PatronRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;

@Component
public class DoiService implements ApplicationContextAware {
	private final Log log = LogFactory.getLog(this.getClass());
	private ApplicationContext applicationContext;
	private PatronRepository patronRepository;
	private MailService mailService;

	private String serverURL;

	public DoiService(
			final PatronRepository patronRepository,
			final MailService mailService,
			@Value("${serverURL}") final String serverURL) {

		this.patronRepository = patronRepository;
		this.mailService = mailService;
		this.serverURL = serverURL;
	}

	public void sendDoiRequestMail(final Patron patron) {
		patron.setDoiCode(createDoiCode(patron));
		patronRepository.save(patron);
		try {
			final String subject = applicationContext.getMessage("email.doi.subject", null, LocaleContextHolder.getLocale());
			mailService.sendDoiMail(patron, subject, serverURL);
		} catch (MessagingException e) {
			log.warn("sendDoiRequestMail: error sending mail", e);
		}
	}

	private String createDoiCode(final Patron user) {
		return RandomCode.createSingle(30) + "x" + user.getId();
	}

	@Override
	public void setApplicationContext(final ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
