/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.service;

import de.andschm.library.backend.data.ResultMessage;
import de.andschm.library.backend.storage.Patron;
import de.andschm.library.backend.storage.PatronRepository;
import de.andschm.library.backend.storage.PushSubscription;
import nl.martijndwars.webpush.Notification;
import nl.martijndwars.webpush.PushService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.jose4j.lang.JoseException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.Security;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Component
public class WebPushService {
	private final Log log = LogFactory.getLog(this.getClass());

	private final PatronRepository patronRepository;
	private final String pushPublicKey;
	private final PushService pushService;

	public WebPushService(
			@Value("${push.public-key}") final String pushPublicKey,
			@Value("${push.private-key}") final String pushPrivateKey,
			final PatronRepository patronRepository) throws GeneralSecurityException {

		this.pushPublicKey = pushPublicKey;
		this.patronRepository = patronRepository;

		if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
			Security.addProvider(new BouncyCastleProvider());
		}

		pushService = new PushService(pushPublicKey, pushPrivateKey, "subject");
	}

	public ResultMessage sendToAll(final String message) {
		final List<Patron> patrons = patronRepository.findDistinctByPushSubscriptionsNotNull();
		for (Patron patron  : patrons) {
			final List<PushSubscription> pushSubscriptions = patron.getPushSubscriptions();
			final List<PushSubscription> gonePushSubscriptions = new LinkedList<>();
			for (PushSubscription pushSubscription : pushSubscriptions) {
				if (!sendPushMessage(pushSubscription, message)) {
					gonePushSubscriptions.add(pushSubscription);
				}
			}
			if (!gonePushSubscriptions.isEmpty()) {
				pushSubscriptions.removeAll(gonePushSubscriptions);
				patronRepository.save(patron);
			}
		}
		return new ResultMessage(true, "");
	}

	public boolean sendPushMessage(PushSubscription sub, String payload) {
		try {
			final Notification notification = new Notification(sub.getEndpoint(), sub.getP256dh(), sub.getAuth(), payload);
			final HttpResponse response = pushService.send(notification);
			final int statusCode = response.getStatusLine().getStatusCode();
			log.debug(String.format("sendPushMessage %d: status = %d", sub.getId(), statusCode));
			//		pushService.sendAsync(notification);
			return statusCode != HttpStatus.SC_GONE;
		} catch (IOException | GeneralSecurityException | ExecutionException | JoseException | InterruptedException e) {
			e.printStackTrace();
			return false;
		}
	}

	public String getPushPublicKey() {
		return pushPublicKey;
	}
}
