/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.service;

import de.andschm.library.backend.RandomCode;
import de.andschm.library.backend.storage.InvitationKey;
import de.andschm.library.backend.storage.InvitationKeyRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class InvitationCodeService {
	private final InvitationKeyRepository invitationKeyRepository;

	public InvitationCodeService(final InvitationKeyRepository invitationKeyRepository) {
		this.invitationKeyRepository = invitationKeyRepository;
	}

	public List<InvitationKey> getUnusedPublicCodes() {
		return invitationKeyRepository.findAllByOwnerIsNullAndUsedByIsNull();
	}

	public void generatePublicCode() {
		final InvitationKey invitationKey = new InvitationKey();
		invitationKey.setKeyValue(RandomCode.createSingle(InvitationKey.KEY_SIZE));
		invitationKeyRepository.save(invitationKey);
	}
}
