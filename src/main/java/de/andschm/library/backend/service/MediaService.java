/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.service;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;
import de.andschm.library.backend.MediaType;
import de.andschm.library.backend.data.MediumInfo;
import de.andschm.library.backend.storage.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component
public class MediaService implements ApplicationContextAware {
	public static final String LOCAL_IMAGE_KEY = "LOCAL";
	public static final String RESULT_SUCCESS = "ok";

	private final Log log = LogFactory.getLog(this.getClass());
	private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyy-MM-dd HH:mm:ss Z");
	private final MediaRepository mediaRepository;
	private final CategoryRepository categoryRepository;
	private final WebPushService pushService;
	private final PatronRepository patronRepository;
	private final MailService mailService;
	private ApplicationContext applicationContext;

	public MediaService(final MediaRepository mediaRepository, final CategoryRepository categoryRepository,
						final WebPushService pushService, final PatronRepository patronRepository, final MailService mailService) {

		this.mediaRepository = mediaRepository;
		this.categoryRepository = categoryRepository;
		this.pushService = pushService;
		this.patronRepository = patronRepository;
		this.mailService = mailService;
	}

	private List<MediumInfo> createMediumInfos(final List<Medium> media) {
		final var mediumInfoList = new LinkedList<MediumInfo>();
		for (final var medium : media) {
			final var mediumInfo = new MediumInfo();
			mediumInfoList.add(mediumInfo);
			mediumInfo.setId(medium.getId());
			mediumInfo.setType(medium.getType());
			final var imagePath = createImagePath(medium.getId(), medium.getImageUrl());
			mediumInfo.setImagePath(imagePath);
			mediumInfo.setAuthor(medium.getAuthor());
			mediumInfo.setTitle(medium.getTitle());
			mediumInfo.setLoanedOut(medium.getLoanedBy() != null);
			mediumInfo.setReleaseDate(medium.getReleaseDate());
			mediumInfo.setCreatedDate(medium.getCreatedDate());
		}
		return mediumInfoList;
	}

	private String createImagePath(final Integer imageId, final String imageUrl) {
		return LOCAL_IMAGE_KEY.equals(imageUrl) ? "/image/" + imageId : imageUrl;
	}

	public Page<MediumInfo> search(final Pageable pageRequest, final String keywords) {
		final var media = mediaRepository
				.findAllByTitleContainsOrOverviewContainsOrAuthorContainsOrPublisherContainsOrderByTitle(pageRequest, keywords, keywords,
						keywords, keywords);
		final var mediumInfoList = createMediumInfos(media.getContent());
		return new PageImpl<>(mediumInfoList, pageRequest, media.getTotalElements());
	}

	public Optional<Medium> getById(final int id) {
		return mediaRepository.findById(id);
	}

	public Medium saveMedium(final Medium medium) {
		final var currentMediumOptional = mediaRepository.findById(medium.getId());
		currentMediumOptional.ifPresent(currentMedium -> medium.setCreatedDate(currentMedium.getCreatedDate()));
		return mediaRepository.save(medium);
	}

	public List<MediumInfo> borrowList() {
		final var media = mediaRepository.getByLoanedByIsNotNullOrderByLoanDateDesc();
		return createMediumInfos(media);
	}

	public void returnMedium(final Integer id) {
		if (id != null) {
			final var mediumOptional = mediaRepository.findById(id);
			mediumOptional.ifPresent(medium -> {
				medium.setLoanedBy(null);
				medium.setLoanDate(null);
				mediaRepository.save(medium);
			});
		}
	}

	public List<MediumInfo> lastImportedMedia() {
		final var lastImportedMedium = mediaRepository.findFirstByOrderByCreatedDateDesc();
		final var lastImportedDate = lastImportedMedium.orElseGet(Medium::new).getCreatedDate();
		return mediaOfDay(lastImportedDate);
	}

	public List<MediumInfo> mediaOfDay(final Date day) {
		final var c = Calendar.getInstance();
		if (day != null) {
			c.setTime(day);
		}
		c.set(Calendar.HOUR_OF_DAY, 0);
		c.set(Calendar.MINUTE, 0);
		c.set(Calendar.SECOND, 0);
		c.set(Calendar.MILLISECOND, 0);
		final var lastImportedMedia = mediaRepository.getByCreatedDateAfterOrderByTitle(c.getTime());
		return createMediumInfos(lastImportedMedia);
	}

	public String importMediaFromCSV(final InputStream input) {
		final var media = new LinkedList<Medium>();
		final var reader = new InputStreamReader(input, StandardCharsets.UTF_8);
		final var csvReader = new CSVReader(reader);

		try {
			csvReader.readNext(); // ignore column caption line
			String[] nextLine;
			while ((nextLine = csvReader.readNext()) != null) {
				final var medium = new Medium();
				medium.setAuthor(nextLine[8]);
				medium.setCountryCode(nextLine[34]);
				medium.setEan(nextLine[15]);
				medium.setGenre(nextLine[23]);
				medium.setImageUrl(nextLine[16]);
				medium.setIsbn(nextLine[28]);
				medium.setLanguage(nextLine[52]);
				medium.setNumPages(StringUtils.isNotEmpty(nextLine[47]) ? Integer.valueOf(nextLine[47]) : null);
				medium.setOverview(nextLine[25]);
				medium.setOwner(nextLine[17]);
				medium.setPublisher(nextLine[61]);
				medium.setReleaseDate(formatDate(nextLine[18]));
				medium.setRevision(nextLine[6]);
				medium.setTitle(nextLine[54]);
				medium.setType(MediaType.valueOf(nextLine[5].toUpperCase()));
				medium.setActors(nextLine[45]);
				medium.setDuration(StringUtils.isNotEmpty(nextLine[51]) ? Integer.valueOf(nextLine[51]) : null);
				final var categoryNames = nextLine[44].split(",");
				for (final var cName : categoryNames) {
					final var categoryName = cName.trim();
					final var categoryOptional = categoryRepository.findByName(categoryName);
					var category = categoryOptional.orElseGet(() -> {
						var newCategory = new Category(categoryName);
						newCategory = categoryRepository.save(newCategory);
						return newCategory;
					});
					medium.getCategories().add(category);
				}
				var newMedium = mediaRepository.save(medium);
				media.add(newMedium);
			}
			csvReader.close();

			if (!media.isEmpty()) {
				final var message = applicationContext.getMessage("push.newMedia.message", null, LocaleContextHolder.getLocale());
				pushService.sendToAll(message);

				final var recipients = patronRepository.findByEmailNotificationTrue();
				final var subject = applicationContext.getMessage("email.newMedia.subject", null, LocaleContextHolder.getLocale());
				sendMail(media, recipients, subject);
			}

			return RESULT_SUCCESS;
		} catch (IOException | CsvValidationException e) {
			log.error("importMediaFromCSV failed", e);
			return "storing";
		}
	}

	private Date formatDate(final String date) {
		try {
			return StringUtils.isNotEmpty(date) ? dateFormat.parse(date) : null;
		} catch (ParseException e) {
			log.warn("upload", e);
			return null;
		}
	}

	private void sendMail(final LinkedList<Medium> media, final List<Patron> recipients, final String subject) {
		try {
			mailService.sendNewMediaMail(recipients, subject, media);
		} catch (MessagingException e) {
			log.warn("importMediaFromCSV: error sending mail", e);
		}
	}

	@Override
	public void setApplicationContext(final ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
