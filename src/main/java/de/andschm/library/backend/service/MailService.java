/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.service;

import de.andschm.library.backend.storage.Medium;
import de.andschm.library.backend.storage.Patron;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Component
public class MailService {
	private static final int NO_OF_SEND_MAIL_SERVICE_THREADS = 20;
	private static final String ENCODING = "UTF-8";

	private final Log log = LogFactory.getLog(this.getClass());


	private final ScheduledExecutorService asynchronousSendMailService = Executors.newScheduledThreadPool(NO_OF_SEND_MAIL_SERVICE_THREADS);
	private final JavaMailSender mailSender;
	private final TemplateEngine templateEngine;
	private final String mailFrom;

	public MailService(final JavaMailSender mailSender, @Value("${spring.mail.from}") final String mailFrom,
					   @Qualifier("emailTemplateEngine") final TemplateEngine templateEngine) {

		this.mailSender = mailSender;
		this.mailFrom = mailFrom;
		this.templateEngine = templateEngine;
	}

	public void sendBorrowMail(final Patron patron, final String subject, final Medium... media) throws MessagingException {
		final var ctx = new Context(LocaleContextHolder.getLocale());
		ctx.setVariable("patron", patron);
		ctx.setVariable("borrowDate", new Date());
		ctx.setVariable("borrowedMedia", media);

		final var mimeMessage = mailSender.createMimeMessage();
		final var message = new MimeMessageHelper(mimeMessage, ENCODING);
		message.setSubject(subject);
		message.setFrom(new InternetAddress(mailFrom));
		message.setTo(patron.getEmail());

		final var htmlContent = templateEngine.process("html/borrow_email", ctx);
		message.setText(htmlContent, true); // true = isHtml

		sendAsynchronousMail(mimeMessage);
	}

	public void sendDoiMail(final Patron patron, final String subject, final String serverUrl) throws MessagingException {
		final var ctx = new Context(LocaleContextHolder.getLocale());
		ctx.setVariable("userName", patron.getEmail());
		ctx.setVariable("serverUrl", serverUrl);
		ctx.setVariable("doiCode", patron.getDoiCode());

		final var mimeMessage = mailSender.createMimeMessage();
		final var message = new MimeMessageHelper(mimeMessage, ENCODING);
		message.setSubject(subject);
		message.setFrom(new InternetAddress(mailFrom));
		message.setTo(patron.getEmail());

		final var htmlContent = templateEngine.process("html/doiRequestMail", ctx);
		message.setText(htmlContent, true); // true = isHtml

		sendAsynchronousMail(mimeMessage);
	}

	public void sendNewMediaMail(final List<Patron> patrons, final String subject, final List<Medium> media) throws MessagingException {
		final var ctx = new Context(LocaleContextHolder.getLocale());
		ctx.setVariable("importDate", new Date());
		ctx.setVariable("newMedia", media);
		final String htmlContent = templateEngine.process("html/newMediaMail", ctx);

		for (final var patron : patrons) {
			final var mimeMessage = mailSender.createMimeMessage();
			final var message = new MimeMessageHelper(mimeMessage, ENCODING);
			message.setSubject(subject);
			message.setFrom(new InternetAddress(mailFrom));
			message.setTo(patron.getEmail());
			message.setText(htmlContent, true); // true = isHtml
			sendAsynchronousMail(mimeMessage);
		}
	}

	private void sendAsynchronousMail(final MimeMessage mimeMessage) {
		asynchronousSendMailService.submit(() -> {
			try {
				mailSender.send(mimeMessage);
			} catch (MailException e) {
				log.error("sendASynchronousMail: ", e);
			}
		});
	}
}
