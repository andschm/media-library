/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

public class RandomCode {
	private static final Random random = new Random(System.nanoTime() + Runtime.getRuntime().freeMemory());
	private static final char[] possibleChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789".toCharArray();

	private RandomCode() {
		// NOP
	}

	public static String createSingle(final int size) {
		char[] code = new char[size];
		fillRandomCode(size, code);

		return String.valueOf(code);
	}

	private static void fillRandomCode(int size, char[] code) {
		IntStream.range(0, size).forEach(i -> code[i] = possibleChars[random.nextInt(possibleChars.length)]);
	}

	public static List<String> createMultiple(final int size, final long count) {
		final List<String> codes = new LinkedList<>();
		char[] code = new char[size];
		for (int i = 0; i < count; i++) {
			fillRandomCode(size, code);
			codes.add(String.valueOf(code));
		}

		return codes;
	}
}
