/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.security;

import de.andschm.library.backend.storage.Patron;
import de.andschm.library.backend.storage.PatronRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AppUserDetailsService implements UserDetailsService {
	private final PatronRepository patronRepository;

	public AppUserDetailsService(final PatronRepository patronRepository) {
		this.patronRepository = patronRepository;
	}

	@Override
	public UserDetails loadUserByUsername(final String username) {
		final Optional<Patron> patronOptional = patronRepository.findByEmailAndAccountLocked(username, false);

		return new LibraryUserDetails(patronOptional.orElseThrow(() -> new UsernameNotFoundException("No user with eMail: " + username)));
	}
}
