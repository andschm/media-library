/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.security;

import de.andschm.library.backend.data.DataWithAuthorities;
import de.andschm.library.backend.data.ResultMessage;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api/v1/security")
public class SecurityController implements ApplicationContextAware {
	private ApplicationContext applicationContext;

	@RequestMapping(value="/login-page", method = RequestMethod.GET)
	public ResultMessage apiLoginPage(final HttpServletResponse response) {
		response.setStatus(HttpStatus.UNAUTHORIZED.value());
		return new ResultMessage(false, HttpStatus.UNAUTHORIZED.getReasonPhrase());
	}

	@RequestMapping(value = "/authentication-failure", method = RequestMethod.GET)
	public ResultMessage apiAuthenticationFailure() {
		// return HttpStatus.OK to let your front-end know the request completed (401 would go back to login again, loops not good)
		final String message = applicationContext.getMessage("security.authentication.failed", null, LocaleContextHolder.getLocale());
		return new ResultMessage(false, message);
	}

	@RequestMapping(value="/default-target", method = RequestMethod.GET)
	public DataWithAuthorities<String> apiDefaultTarget() {
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return new DataWithAuthorities<>("", authentication.getAuthorities());
	}

	@Override
	public void setApplicationContext(final ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

}
