package de.andschm.library.backend.security;

import de.andschm.library.backend.storage.PatronRepository;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class AuthenticationSuccessEventListener implements ApplicationListener<AuthenticationSuccessEvent> {
	private final Log log = LogFactory.getLog(this.getClass());
	private final PatronRepository patronRepository;

	public AuthenticationSuccessEventListener(final PatronRepository patronRepository) {
		this.patronRepository = patronRepository;
	}

	@Override
	public void onApplicationEvent(final AuthenticationSuccessEvent event) {
		final var authentication = event.getAuthentication();
		final LibraryUserDetails principal = (LibraryUserDetails) authentication.getPrincipal();
		final var patron = principal.getPatron();
		patron.setLastLogin(new Date());
		patronRepository.save(patron);
		log.debug("onApplicationEvent - last login saved");
	}
}
