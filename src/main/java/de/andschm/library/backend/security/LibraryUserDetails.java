/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.security;

import de.andschm.library.backend.storage.Patron;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

public class LibraryUserDetails implements UserDetails {
	private final Patron patron;
	private final Collection<? extends GrantedAuthority> authorities;

	public LibraryUserDetails(final Patron patron) {
		this.patron = patron;
		final GrantedAuthority authority = new SimpleGrantedAuthority(patron.getRole().name());
		authorities = Collections.singletonList(authority);
	}

	Patron getPatron() {
		return patron;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public String getPassword() {
		return patron.getPasswordHash();
	}

	@Override
	public String getUsername() {
		return patron.getEmail();
	}

	@Override
	public boolean isAccountNonExpired() {
		return !patron.isAccountExpired();
	}

	@Override
	public boolean isAccountNonLocked() {
		return !patron.isAccountLocked();
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return !patron.isPasswordExpired();
	}

	@Override
	public boolean isEnabled() {
		return patron.isEnabled();
	}
}
