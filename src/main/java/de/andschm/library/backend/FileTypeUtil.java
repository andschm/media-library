/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class FileTypeUtil {
	private int prefixLength = 4;
	private Map<String, List<byte[]>> types;
	private int maxHeaderLength = 0;

	public FileTypeUtil() {
		super();
	}

	public FileTypeUtil(int prefixLength) {
		this();
		this.prefixLength = prefixLength;
	}

	/**
	 * überprüft, ob <code>fileHeader</code> als Value in der <code>types</code> <code>Map</code> vorhanden ist. Wenn
	 * ja, wird <code>true</code> zurückgegeben. In allen anderen Fällen wird <code>false</code> zurückgegeben.
	 *
	 * @param fileHeader Die ersten Bytes aus der Datei.
	 * @return true wenn fileHeader ein bekannter header ist.
	 */
	public boolean checkType(byte[] fileHeader) {
		final byte[] offsetHeader = Arrays.copyOfRange(fileHeader, prefixLength, fileHeader.length);
		boolean valid = false;
		for (Entry<String, List<byte[]>> entry : types.entrySet()) {
			for (byte[] header : entry.getValue()) {
				if (offsetHeader.length >= header.length) {
					valid = true;
					for (int i = 0; i < header.length; i++) {
						if (header[i] != offsetHeader[i]) {
							valid = false;
							break;
						}
					}
				}
				if (valid) {
					break;
				}
			}
			if (valid) {
				break;
			}
		}
		return valid;
	}

	/**
	 * Setzt die <code>types</code> <code>Map</code>.<br>
	 * Die übergebenen Map wird modifiziert. Die übergebene Map hat als key und value den Typ String. Beginnt der value
	 * String mit "$", so wird er als Folge von Hex-Ziffern interpretiert und jeweils zwei Hex-Ziffern werden zu einem
	 * <code>byte</code> umgewandelt. Die so erzeugten Bytes werden als Byte-Array unter dem key abgelegt. Ohne
	 * führendes "$" wird der String mit <code>getBytes</code> in ein Byte-Array umgewandelt und unter dem key abgelegt.
	 *
	 * @param typesInput the types to set
	 */
	public void setTypes(Map<String, String> typesInput) {
		final Map<String, List<byte[]>> newTypes = new HashMap<>();
		for (Entry<String, String> entry : typesInput.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			byte[] byteValue;
			if (value.startsWith("$")) {
				// Angabe in HEX, also String parsen.
				value = value.substring(1);
				int length = value.length() / 2;
				byteValue = new byte[length];
				for (int i = 0; i < length; i++) {
					String part = value.substring(i * 2, i * 2 + 2);
					byteValue[i] = (byte) Integer.parseInt(part, 16);
				}
			} else {
				// Angabe als String, also String in Byte-Array umwandeln.
				byteValue = value.getBytes(StandardCharsets.US_ASCII);
			}
			addByteValue(newTypes, key, byteValue);
			if (byteValue.length > maxHeaderLength) {
				maxHeaderLength = byteValue.length;
			}
		}
		this.types = newTypes;
	}

	private void addByteValue(Map<String, List<byte[]>> types, String key, byte[] byteValue) {
		List<byte[]> values = types.computeIfAbsent(key, k -> new LinkedList<>());
		values.add(byteValue);
	}

	/**
	 * @return the maxHeaderLength
	 */
	public int getMaxHeaderLength() {
		return maxHeaderLength;
	}
}
