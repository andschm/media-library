/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.data;

import java.util.List;

public class AccountPageData {
	private String email;
	private boolean emailNotification;
	private List<String> invitationKeys;
	private String pushPublicKey;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isEmailNotification() {
		return emailNotification;
	}

	public void setEmailNotification(boolean emailNotification) {
		this.emailNotification = emailNotification;
	}

	public List<String> getInvitationKeys() {
		return invitationKeys;
	}

	public void setInvitationKeys(List<String> invitationKeys) {
		this.invitationKeys = invitationKeys;
	}

	public String getPushPublicKey() {
		return pushPublicKey;
	}

	public void setPushPublicKey(String pushPublicKey) {
		this.pushPublicKey = pushPublicKey;
	}
}
