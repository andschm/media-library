/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.data;

import de.andschm.library.backend.service.ConfigurationService;
import de.andschm.library.backend.storage.Configuration;
import de.andschm.library.backend.storage.InvitationKey;
import de.andschm.library.backend.storage.InvitationKeyRepository;
import de.andschm.library.backend.storage.PatronRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import java.util.Optional;

@Component
public class NewAccountDataValidator extends AccountDataValidator {
	private static final String E_MAIL_FIELD = "email";
	private static final String INVITATION_KEY_FIELD = "invitationKey";

	private final ConfigurationService configurationService;
	private final InvitationKeyRepository invitationKeyRepository;
	private final PatronRepository patronRepository;

	@Value("${accountData.eMail.regexp}")
	private String eMailRegExp;

	public NewAccountDataValidator(
			final ConfigurationService configurationService,
			final InvitationKeyRepository invitationKeyRepository,
			final PatronRepository patronRepository) {

		this.configurationService = configurationService;
		this.invitationKeyRepository = invitationKeyRepository;
		this.patronRepository = patronRepository;
	}

	@Override
	public void validateInternal(final Object target, final Errors errors) {
		final var newAccountData = (NewAccountData) target;
		if (StringUtils.isEmpty(newAccountData.getEmail())) {
			errors.rejectValue(E_MAIL_FIELD, "error.newAccountData.email.empty");
		} else if (!newAccountData.getEmail().matches(eMailRegExp)) {
			errors.rejectValue(E_MAIL_FIELD, "error.accountData.email.pattern");
		} else if (patronRepository.findByEmail(newAccountData.getEmail()).isPresent()) {
			errors.rejectValue(E_MAIL_FIELD, "error.accountData.email.used");
		}

		if (!newAccountData.getPassword().equals(newAccountData.getPasswordRepeat())) {
			errors.rejectValue("passwordRepeat", "error.accountData.passwordRepeat.notEqual");
		}

		if (!newAccountData.isAcceptTerms()) {
			errors.rejectValue("acceptTerms", "error.newAccountData.acceptTerms.unchecked");
		}

		if (Configuration.ACTIVE.equals(configurationService.getTextConfiguration(Configuration.INVITATION_NEEDED, ""))) {
			if (StringUtils.isEmpty(newAccountData.getInvitationKey())) {
				errors.rejectValue(INVITATION_KEY_FIELD, "error.newAccountData.invitationKey.empty");
			} else {
				final Optional<InvitationKey> invitationKeyOptional =
						invitationKeyRepository.findByKeyValue(newAccountData.getInvitationKey());
				invitationKeyOptional.ifPresentOrElse(invitationKeyFromDb -> {
					if (!invitationKeyFromDb.isUnused()) {
						errors.rejectValue(INVITATION_KEY_FIELD, "error.newAccountData.invitationKey.used");
					}
				}, () -> errors.rejectValue(INVITATION_KEY_FIELD, "error.newAccountData.invitationKey.invalid"));
			}
		}
	}

}
