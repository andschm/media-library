/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.data;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

/**
 * Validator for EditAccountData object.
 * <p>
 * Created by andschm on 1.1.2018.
 */
@Component
public class EditAccountDataValidator extends AccountDataValidator {
	@Value("${accountData.eMail.regexp}")
	private String eMailRegExp;

	@Override
	public void validateInternal(Object target, Errors errors) {
		final EditAccountData editAccountData = (EditAccountData) target;
		final String newPasword = editAccountData.getNewPassword();
		if (!newPasword.equals(editAccountData.getNewPasswordRepeat())) {
			errors.rejectValue("newPasswordRepeat", "error.accountData.passwordRepeat.notEqual");
		} else if (newPasword.length() > 0 && newPasword.length() < 8) {
			errors.rejectValue("newPassword", "error.accountData.password.size");
		}
	}
}
