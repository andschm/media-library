/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.data;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public abstract class AccountDataValidator implements Validator {
	@Override
	public final boolean supports(Class<?> clazz) {
		return NewAccountData.class.equals(clazz) || EditAccountData.class.equals(clazz) || String.class.equals(clazz);
	}

	@Override
	public final void validate(Object target, Errors errors) {
		try {
			validateInternal(target, errors);
		} catch (ClassCastException cce) {
			// NOP
		}

	}

	protected abstract void validateInternal(Object target, Errors errors);
}
