/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.data;

import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class DataWithAuthorities<T> {
	private final T data;
	private final Collection<? extends GrantedAuthority> authorities;

	public DataWithAuthorities(final T data, final Collection<? extends GrantedAuthority> authorities) {
		this.data = data;
		this.authorities = authorities;
	}

	public T getData() {
		return data;
	}

	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}
}
