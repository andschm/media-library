/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class NewAccountData {
	@NotNull
	@Size(max = 255)
	private String email;
	@NotNull
	@Size(min = 8)
	private String password;
	@NotNull
	@Size(min = 8)
	private String passwordRepeat;
	private String invitationKey;
	private boolean acceptTerms;

	public void clearAllFieldsButInvitationKey() {
		email = "";
		password = "";
		passwordRepeat = "";
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordRepeat() {
		return passwordRepeat;
	}

	public void setPasswordRepeat(String passwordRepeat) {
		this.passwordRepeat = passwordRepeat;
	}

	public String getInvitationKey() {
		return invitationKey;
	}

	public void setInvitationKey(String invitationKey) {
		this.invitationKey = invitationKey;
	}

	public boolean isAcceptTerms() {
		return acceptTerms;
	}

	public void setAcceptTerms(boolean acceptTerms) {
		this.acceptTerms = acceptTerms;
	}
}
