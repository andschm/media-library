/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.controller;

import de.andschm.library.backend.service.ImageService;
import de.andschm.library.backend.storage.Image;
import de.andschm.library.backend.storage.Medium;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;

@RestController
public class ImageDownloadController {
	private final Log log = LogFactory.getLog(this.getClass());

	private final ImageService imageService;

	public ImageDownloadController(ImageService imageService) {
		this.imageService = imageService;
	}

	@GetMapping("/image/{mediumId}")
	public void download(@PathVariable Integer mediumId, HttpServletResponse response) {
		final Medium medium = new Medium();
		medium.setId(mediumId);
		final Image image = imageService.getImageForMedium(medium);
		if (image != null) {
			final String contentType;
			final String extension = FilenameUtils.getExtension(image.getOriginalFileName()).toLowerCase();
			switch (extension) {
				case "jpg":
				case "jpeg":
					contentType = "image/jpeg";
					break;
				case "tif":
				case "tiff":
					contentType = "image/tiff";
					break;
				case "png":
					contentType = "image/png";
					break;
				default:
					contentType = "image";
					break;
			}
			response.setHeader("Content-Type", contentType);
			try {
				final long length = image.getData().length;
				if (length > 0) {
					response.setHeader("Content-Length", String.valueOf(length));
				}
				ByteArrayInputStream in = new ByteArrayInputStream(image.getData());
				IOUtils.copy(in, response.getOutputStream());
				response.flushBuffer();
			} catch (IOException e) {
				log.error("download: reading image failed for image id: " + image.getId(), e);
				try {
					response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value());
				} catch (IOException ie) {
					log.error("download: unable to send error response", ie);
				}
			}
		} else {
			response.setStatus(HttpStatus.NOT_FOUND.value());
		}
	}

}
