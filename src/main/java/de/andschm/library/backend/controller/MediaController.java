/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.controller;

import de.andschm.library.backend.data.BorrowData;
import de.andschm.library.backend.data.DataWithAuthorities;
import de.andschm.library.backend.data.MediumInfo;
import de.andschm.library.backend.data.ResultMessage;
import de.andschm.library.backend.service.MediaService;
import de.andschm.library.backend.service.PatronService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/v1/media")
public class MediaController implements ApplicationContextAware {
	private ApplicationContext applicationContext;
	private final MediaService mediaService;
	private final PatronService patronService;

	public MediaController(final MediaService mediaService, final PatronService patronService) {
		this.mediaService = mediaService;
		this.patronService = patronService;
	}

	@GetMapping("search")
	public DataWithAuthorities<Page<MediumInfo>> search(final Pageable pageRequest, @RequestParam final String keywords) {

		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return new DataWithAuthorities<>(mediaService.search(pageRequest, keywords), authentication.getAuthorities());
	}

	@PostMapping("borrow")
	public ResultMessage borrow(@Valid final BorrowData borrowData, final Errors errors) {
		final ResultMessage message;
		if (errors.hasErrors()) {
			message = new ResultMessage(false, applicationContext.getMessage(errors.getFieldError(), LocaleContextHolder.getLocale()));
		} else {
			final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			if (patronService.borrow(authentication.getName(), borrowData.getItemId())) {
				message = new ResultMessage(true, "OK");
			} else {
				message = new ResultMessage(false,
						applicationContext.getMessage("borrow.itemId.invalid", null, LocaleContextHolder.getLocale()));
			}
		}
		return message;
	}

	@GetMapping("newArrivals")
	public List<MediumInfo> findNewArrivals() {
		return mediaService.lastImportedMedia();
	}

	@Override
	public void setApplicationContext(final ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
