/*
    Java web application for media library management.
    Copyright (C) 2018  Andreas Schmitz

    This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
    published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.
*/
package de.andschm.library.backend.controller;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import de.andschm.library.backend.data.*;
import de.andschm.library.backend.service.ConfigurationService;
import de.andschm.library.backend.service.PatronService;
import de.andschm.library.backend.service.PatronService.ResponseStatus;
import de.andschm.library.backend.storage.Configuration;
import de.andschm.library.backend.storage.Patron;
import de.andschm.library.backend.storage.PushSubscription;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.StringReader;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@RestController
@RequestMapping("/api/v1/account")
public class AccountController implements ApplicationContextAware {
	private final PasswordEncoder passwordEncoder;
	private final PatronService patronService;
	private final ConfigurationService configurationService;
	private final NewAccountDataValidator newAccountDataValidator;
	private final EditAccountDataValidator editAccountDataValidator;
	private ApplicationContext applicationContext;

	public AccountController(final PasswordEncoder passwordEncoder, final PatronService patronService,
							 final ConfigurationService configurationService, final NewAccountDataValidator newAccountDataValidator,
							 final EditAccountDataValidator editAccountDataValidator) {

		this.passwordEncoder = passwordEncoder;
		this.patronService = patronService;
		this.configurationService = configurationService;
		this.newAccountDataValidator = newAccountDataValidator;
		this.editAccountDataValidator = editAccountDataValidator;
	}

	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		binder.addValidators(newAccountDataValidator, editAccountDataValidator);
	}

	@PostMapping("new")
	public ResultMessage newAccount(@Valid final NewAccountData newAccountData, final Errors errors) {
		ResultMessage message;
		if (!errors.hasErrors()) {
			try {
				patronService.createPatron(newAccountData);
				message = new ResultMessage(true, "OK");
			} catch (DataAccessException dae) {
				message = new ResultMessage(false,
						applicationContext.getMessage("account.new.error.save", null, LocaleContextHolder.getLocale()));
			}
		} else {
			message = new ResultMessage(false, applicationContext.getMessage(errors.getFieldError(), LocaleContextHolder.getLocale()));
		}
		return message;
	}

	@GetMapping("new")
	public ResultMessage newAccountCheck() {
		return new ResultMessage(true, configurationService.getTextConfiguration(Configuration.INVITATION_NEEDED, ""));
	}

	@GetMapping("doiConfirmation/{doiCode}")
	public ResultMessage doiConfirmation(@PathVariable final String doiCode) {
		final ResponseStatus responseStatus = patronService.enableUser(doiCode);
		final String message;
		final boolean success = ResponseStatus.OK == responseStatus || ResponseStatus.ALREADY_CONFIRMED == responseStatus;
		if (success) {
			message = "";
		} else if (ResponseStatus.NO_DOI_CODE == responseStatus) {
			message = applicationContext.getMessage("account.confirmation.error.nodoicode", null, LocaleContextHolder.getLocale());
		} else {
			message = applicationContext.getMessage("account.confirmation.error.nouser", null, LocaleContextHolder.getLocale());
		}
		return new ResultMessage(success, message);
	}

	@PostMapping("edit")
	public ResultMessage editAccount(@Valid final EditAccountData editAccountData, final Errors errors) {
		final AtomicReference<ResultMessage> message = new AtomicReference<>();
		if (!errors.hasErrors()) {
			getPatron().ifPresentOrElse(patron -> {
				boolean changed = false;
				boolean wrongPassword = false;
				if (passwordChangeRequested(editAccountData)) {
					if (passwordEncoder.matches(editAccountData.getPassword(), patron.getPasswordHash())) {
						patron.setPasswordHash(passwordEncoder.encode(editAccountData.getNewPassword()));
						changed = true;
					} else {
						message.set(new ResultMessage(false,
								applicationContext.getMessage("error.accountData.password", null, LocaleContextHolder.getLocale())));
						wrongPassword = true;
					}
				}
				if (!wrongPassword) {
					if (patron.isEmailNotification() != editAccountData.isEmailNotification()) {
						patron.setEmailNotification(editAccountData.isEmailNotification());
						changed = true;
					}
					if (changed) {
						patronService.savePatron(patron);
						message.set(new ResultMessage(true, "OK"));
					} else {
						message.set(new ResultMessage(true, "NOP"));
					}
				}
			}, () -> message.set(new ResultMessage(false,
					applicationContext.getMessage("error.accountData.patron", null, LocaleContextHolder.getLocale()))));
		} else {
			message.set(new ResultMessage(false, applicationContext.getMessage(errors.getFieldError(), LocaleContextHolder.getLocale())));
		}
		return message.get();
	}

	private boolean passwordChangeRequested(final EditAccountData editAccountData) {
		return StringUtils.isNotBlank(editAccountData.getNewPassword());
	}

	private Optional<Patron> getPatron() {
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return patronService.getByEMail(authentication.getName());
	}

	@PostMapping("subscribe")
	public ResultMessage subscribePush(@RequestBody final String encodedSubscriptionJSON) {
		final PushSubscription subscription = createPushSubscription(encodedSubscriptionJSON);
		getPatron().ifPresent(patron -> {
			patron.addPushSubscription(subscription);
			patronService.savePatron(patron);
		});
		return new ResultMessage(true, "");
	}

	private PushSubscription createPushSubscription(@RequestBody String encodedSubscriptionJSON) {
		final String subscriptionJSON = URLDecoder.decode(encodedSubscriptionJSON, StandardCharsets.UTF_8);
		final JsonReader jsonReader = new JsonReader(new StringReader(subscriptionJSON));
		jsonReader.setLenient(true);
		final JsonObject element = (JsonObject) JsonParser.parseReader(jsonReader);
		final PushSubscription subscription = new PushSubscription();
		subscription.setEndpoint(element.get("endpoint").getAsString());
		final JsonObject keys = (JsonObject) element.get("keys");
		subscription.setAuth(keys.get("auth").getAsString());
		subscription.setP256dh(keys.get("p256dh").getAsString());
		return subscription;
	}

	@GetMapping("data")
	public DataWithAuthorities<AccountPageData> accountData() {
		final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return new DataWithAuthorities<>(patronService.getAccountData(authentication.getName()), authentication.getAuthorities());
	}

	@Override
	public void setApplicationContext(final ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
