package de.andschm.library;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.io.IOException;
import java.nio.charset.Charset;

@ControllerAdvice
public class NotFoundHandler {
	private static final String ADMIN_PATH = "/admin";

	private Resource defaultPathResource;

	public NotFoundHandler(@Value("${spa.default-path}") final Resource defaultPathResource) {
		this.defaultPathResource = defaultPathResource;
	}

	@ExceptionHandler
	public ResponseEntity<String> renderDefaultPage(NoHandlerFoundException nhfe) {
		final String requestURL = nhfe.getRequestURL();
		final String relativePath;
		if (requestURL.startsWith(ADMIN_PATH)) {
			relativePath = ADMIN_PATH;
		} else {
			relativePath = "";
		}
		try {
			final Resource defaultFileContent = defaultPathResource.createRelative(relativePath + "/index.html");
			String body = StreamUtils.copyToString(defaultFileContent.getInputStream(), Charset.defaultCharset());
			return ResponseEntity.ok().contentType(MediaType.TEXT_HTML).body(body);
		} catch (IOException e) {
			e.printStackTrace();
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("There was an error completing the action.");
		}
	}
}
