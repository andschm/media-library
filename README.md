# Library Manager
Java web application for media library management.

Copyright (C) 2018 Andreas Schmitz

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, see <http://www.gnu.org/licenses/>.

## External resources
- [Library icon](http://www.iconseeker.com/search-icon/desktoon/library-10.html)
- [vue.js](https://vuejs.org/)
- [vue-i18next](https://github.com/panter/vue-i18next)
- [i18next-xhr-backend](https://github.com/i18next/i18next-xhr-backend)
- [Vue Loader](https://vue-loader.vuejs.org/)
- [VueX](https://vuex.vuejs.org/)
- [Vue Router](https://router.vuejs.org/)
- [Single Page Applications with Spring Boot](https://medium.com/@kshep92/single-page-applications-with-spring-boot-b64d8d37015d)
- [webpack](https://webpack.js.org/)