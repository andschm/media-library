ALTER TABLE `patron` ADD COLUMN `created_date` DATETIME(3) NOT NULL default '2018-09-08 19:41:08.000' AFTER `account_locked`;
UPDATE patron SET created_date=ADDDATE('1970-01-01', INTERVAL create_date * 1000 MICROSECOND);
ALTER TABLE `patron` ADD COLUMN `lm_date` DATETIME(3) NOT NULL default '2018-09-08 19:41:08.000' AFTER `last_modified_date`;
UPDATE patron SET lm_date=ADDDATE('1970-01-01', INTERVAL last_modified_date * 1000 MICROSECOND);
ALTER TABLE `patron` DROP COLUMN `create_date`, DROP COLUMN `last_modified_date`;
ALTER TABLE `patron` CHANGE COLUMN `lm_date` `last_modified_date` DATETIME(3) NOT NULL AFTER `e_mail`;
ALTER TABLE `patron` CHANGE COLUMN `created_date` `created_date` DATETIME(3) NOT NULL;

ALTER TABLE `medium` ADD COLUMN `created_date` DATETIME(3) NOT NULL default '2018-09-08 19:41:08.000' AFTER `country_code`;
UPDATE medium SET created_date=ADDDATE('1970-01-01', INTERVAL create_date * 1000 MICROSECOND);
ALTER TABLE `medium` ADD COLUMN `lm_date` DATETIME(3) NOT NULL default '2018-09-08 19:41:08.000' AFTER `last_modified_date`;
UPDATE medium SET lm_date=ADDDATE('1970-01-01', INTERVAL last_modified_date * 1000 MICROSECOND);
ALTER TABLE `medium` DROP COLUMN `create_date`, DROP COLUMN `last_modified_date`;
ALTER TABLE `medium` CHANGE COLUMN `lm_date` `last_modified_date` DATETIME(3) NOT NULL AFTER `language`;
ALTER TABLE `medium` CHANGE COLUMN `created_date` `created_date` DATETIME(3) NOT NULL;

ALTER TABLE `invitation_key` ADD COLUMN `created_date` DATETIME(3) NOT NULL default '2018-09-08 19:41:08.000' AFTER `key_value`;
UPDATE invitation_key SET created_date=ADDDATE('1970-01-01', INTERVAL create_date * 1000 MICROSECOND);
ALTER TABLE `invitation_key` ADD COLUMN `lm_date` DATETIME(3) NOT NULL default '2018-09-08 19:41:08.000' AFTER `last_modified_date`;
UPDATE invitation_key SET lm_date=ADDDATE('1970-01-01', INTERVAL last_modified_date * 1000 MICROSECOND);
ALTER TABLE `invitation_key` DROP COLUMN `create_date`, DROP COLUMN `last_modified_date`;
ALTER TABLE `invitation_key` CHANGE COLUMN `lm_date` `last_modified_date` DATETIME(3) NOT NULL AFTER `created_date`;
ALTER TABLE `invitation_key` CHANGE COLUMN `created_date` `created_date` DATETIME(3) NOT NULL;

ALTER TABLE `image` ADD COLUMN `created_date` DATETIME(3) NOT NULL default '2018-09-08 19:41:08.000' AFTER `id`;
UPDATE image SET created_date=ADDDATE('1970-01-01', INTERVAL create_date * 1000 MICROSECOND);
ALTER TABLE `image` ADD COLUMN `lm_date` DATETIME(3) NOT NULL default '2018-09-08 19:41:08.000' AFTER `last_modified_date`;
UPDATE image SET lm_date=ADDDATE('1970-01-01', INTERVAL last_modified_date * 1000 MICROSECOND);
ALTER TABLE `image` DROP COLUMN `create_date`, DROP COLUMN `last_modified_date`;
ALTER TABLE `image` CHANGE COLUMN `lm_date` `last_modified_date` DATETIME(3) NOT NULL AFTER `data`;
ALTER TABLE `image` CHANGE COLUMN `created_date` `created_date` DATETIME(3) NOT NULL;

ALTER TABLE `configuration` ADD COLUMN `created_date` DATETIME(3) NOT NULL default '2018-09-08 19:41:08.000' AFTER `name`;
UPDATE configuration SET created_date=ADDDATE('1970-01-01', INTERVAL create_date * 1000 MICROSECOND);
ALTER TABLE `configuration` ADD COLUMN `lm_date` DATETIME(3) NOT NULL default '2018-09-08 19:41:08.000' AFTER `last_modified_date`;
UPDATE configuration SET lm_date=ADDDATE('1970-01-01', INTERVAL last_modified_date * 1000 MICROSECOND);
ALTER TABLE `configuration` DROP COLUMN `create_date`, DROP COLUMN `last_modified_date`;
ALTER TABLE `configuration` CHANGE COLUMN `lm_date` `last_modified_date` DATETIME(3) NOT NULL AFTER `created_date`;
ALTER TABLE `configuration` CHANGE COLUMN `created_date` `created_date` DATETIME(3) NOT NULL;

ALTER TABLE `category` ADD COLUMN `created_date` DATETIME(3) NOT NULL default '2018-09-08 19:41:08.000' AFTER `id`;
UPDATE category SET created_date=ADDDATE('1970-01-01', INTERVAL create_date * 1000 MICROSECOND);
ALTER TABLE `category` ADD COLUMN `lm_date` DATETIME(3) NOT NULL default '2018-09-08 19:41:08.000' AFTER `last_modified_date`;
UPDATE category SET lm_date=ADDDATE('1970-01-01', INTERVAL last_modified_date * 1000 MICROSECOND);
ALTER TABLE `category` DROP COLUMN `create_date`, DROP COLUMN `last_modified_date`;
ALTER TABLE `category` CHANGE COLUMN `lm_date` `last_modified_date` DATETIME(3) NOT NULL AFTER `created_date`;
ALTER TABLE `category` CHANGE COLUMN `created_date` `created_date` DATETIME(3) NOT NULL;
